export class FilterQuery {
    private filterQuery: { [index: string]: string; } = {};

    /** Adiciona uma key nova ou a remove caso exista */
    toggle(key: string, value: string): void {
        if (this.filterQuery[key] === value) {
            this.remove(key);
        } else {
            this.set(key, value);
        }
    }

    /** Substitui o value da key diretamente */
    private set(key: string, value: string): void {
        // Como vamos atribuir dinamicamente o valor para essa query,
        // faz sentido que apenas atualizemos o valor, sem verificar a key
        this.filterQuery[key] = value;
    }

    private remove(key: string): void {
        if (this.filterQuery[key]) {
            delete this.filterQuery[key];
        }
    }

    clear(): void {
        this.filterQuery = {};
    }

    getQuery(): string {
        let filterQuery = '';

        const keys = Object.keys(this.filterQuery);

        if (keys.length && keys.length > 0) {
            keys.forEach(key => {
                const value = this.filterQuery[key];
                filterQuery += '&' + key + '=' + value;
            });
        }

        return filterQuery;
    }
}
