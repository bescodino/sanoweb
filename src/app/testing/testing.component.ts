import { Component, ViewChild, OnInit, AfterViewInit, OnDestroy, Renderer2, Inject } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

import * as Chart from 'chart.js';

// Creator Patterns
import 'rxjs/add/observable/fromEvent';

// Transformation Methods
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/take';

import { ApiSettings } from '../settings/api.settings';
import { FactoryMapper } from '../settings/factory.mapper';

import { ChartsLoaderService } from '../services/charts/charts-loader.service';
import { ChartsEventEmitterService } from '../services/charts/charts-event-emitter.service';
import { IChart, IChartEntry, IChartParser, IChartNEW } from '../charts/chart-entities.model';
import {
    ChartLabelTexts, ChartSalesStatus,
    ChartDistributor, ChartUF, ChartBU,
    ChartLandingPage, ChartHtmlInfo
} from '../charts/charts-implementation';

import { ChartsParserFactory } from '../factories/parser/parser-factory.service';
import { ChartsFactory } from '../factories/chart/chart-factory.service';
import { ChartsMetadataFactory } from '../factories/chart/chart-metadata-factory.service';

import { FilterQuery } from '../filter/filter-query.component';
import { ColorsService } from '../services/utility/colors.service';
import { Observable } from 'rxjs/Observable';
import { callLifecycleHooksChildrenFirst } from '@angular/core/src/view/provider';

import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs/Subscription';


@Component({
    selector: 'app-testing',
    templateUrl: './testing.component.html',
    styleUrls: ['./testing.component.css']
})
export class TestingComponent implements OnInit, OnDestroy, AfterViewInit {

    private filterQuery: FilterQuery;

    private settings = new ApiSettings();
    private factoryMapper = new FactoryMapper();

    public testingOne: IChart;
    public testingOneChart: Chart;

    public testChart: IChartNEW;
    public testInnerChart: Chart;

    public testSubscription$;
    public updateSubscription$;
    public testSubscription2$;
    public testListSubscriptions$ = new Array<Observable<any>>();

    // All canvases
    public readonly salesCanvasID = 'sales';
    public readonly testCanvasID = 'test';

    // All Subscriptionseses $
    public salesSubscription$;
    public salesChart: IChartNEW;
    public salesInnerChart: Chart;

    private end$ = this.router.events.filter((event) => event instanceof NavigationEnd);


    constructor(
        private colorsService: ColorsService,
        private chartsParserFactory: ChartsParserFactory,
        private chartsFactory: ChartsFactory,
        private chartsMetadataFactory: ChartsMetadataFactory,
        private chartsLoaderService: ChartsLoaderService<IChart>,
        private eventEmitter: ChartsEventEmitterService,
        private translate: TranslateService,
        private router: Router) {

        this.filterQuery = new FilterQuery();

        eventEmitter.graphClicked$
            .subscribe((item) => this.onChartClicked(item));
    }

    ngOnInit() {
        // this.showLoaders();
        this.testDownload();
        this.loadCharts();

    }

    ngAfterViewInit() {
        // this.loadCards();
        this.subscribeCharts();
    }

    ngOnDestroy() {
        console.log('testing::onDestroy');
        // this.clear();
    }

    onChartClicked(item: any) {
        this.filterQuery.toggle(item.graphLabel, item.item[0]._model.label);
        console.log('chartClicked::query::', JSON.stringify(this.filterQuery.getQuery()));
    }

    loadCharts() {
        // entry.authorized = this.translate
        // .instant('variables.bool.' + entry.authorized);

        this.testListSubscriptions$.push(
            this.chartsLoaderService
                .getAsObservableMOCK(this.settings.rupByNotAtEnd, 'rupbynotatendA')
                .takeUntil(this.end$)
                // .take(1)
                .map(res => {
                    console.log('loadCharts:', JSON.stringify(this.settings.rupByNotAtEnd));

                    const factoryID = this.factoryMapper.getId(this.settings.rupByNotAtEnd);

                    // Parse
                    const content = this.chartsParserFactory
                        .getParser(factoryID)
                        .parse(res, false);

                    // Create
                    this.salesChart = this.salesChart || this.chartsFactory.getChart(factoryID);
                    this.salesChart
                        .setContent(content)
                        .setMetaDataContent(this.chartsMetadataFactory.getMetadata(factoryID)); // get appropriate view model

                    this.salesInnerChart = this.salesChart.renderChart(this.salesCanvasID);
                })
        );
    }

    subscribeCharts() {

    }

    testDownload() {

        const chartCanvasID = 'bar-chart-horizontal';

        // this.settings.rupByNotAtEnd
        // const end$ = this.router.events.filter((event) => event instanceof NavigationEnd);

        this.updateSubscription$ = this.chartsLoaderService
            .getAsObservableMOCK(this.settings.upTop10DistChains, 'upTop10DistC')
            .map(val => {
                console.log('updateSubscription', JSON.stringify(val));

                const factoryID = this.factoryMapper.getId(this.settings.upTop10DistChains);

                const content = this.chartsParserFactory
                    .getParser(factoryID)
                    .parse(val, false);

                this.testInnerChart = this.testChart
                    .updateChart(content, this.chartsMetadataFactory.getMetadata(factoryID));
            });

        this.testSubscription2$ = this.chartsLoaderService
            .getAsObservableMOCK(this.settings.upTop10Dist, 'upTop10DistB')
            .takeUntil(this.end$)
            // .take(1)
            .map(val => {
                const factoryID = this.factoryMapper.getId(this.settings.upTop10Dist);
                // const factoryID = this.settings.upTop10Dist;

                // Parse
                const content = this.chartsParserFactory
                    .getParser(factoryID)
                    .parse(val, false);

                // Create
                this.testChart
                    .setContent(content)
                    .setMetaDataContent(this.chartsMetadataFactory.getMetadata(factoryID)); // get appropriate view model

                this.testInnerChart = this.testChart.renderChart(this.testCanvasID);
            });

        this.testSubscription$ = this.chartsLoaderService
            .getAsObservableMOCK(this.settings.upTop10Dist, 'upTop10DistA')
            .takeUntil(this.end$)
            // .take(1)
            .map(val => {
                const factoryID = this.factoryMapper.getId(this.settings.upTop10Dist);

                // Parse
                const content = this.chartsParserFactory
                    .getParser(factoryID)
                    .parse(val, false);

                // Create
                this.testChart = this.testChart || this.chartsFactory.getChart(factoryID);
                this.testChart
                    .setContent(content)
                    .setMetaDataContent(this.chartsMetadataFactory.getMetadata(factoryID)); // get appropriate view model

                this.testInnerChart = this.testChart.renderChart(this.testCanvasID);
            });



        // Gráfico de testes
        this.testingOneChart = this.createChart();
    }

    onUpdate() {
        this.updateSubscription$.subscribe();
    }

    onSubscribe() {
        this.testSubscription$.subscribe();

        this.testListSubscriptions$.forEach(element => {
            element.subscribe();
        });
    }

    onSubscribe2() {
        this.testSubscription2$.subscribe();
    }



    createChart(): Chart {

        const canvas: any = document.getElementById('testingOne');
        const context = canvas.getContext('2d');

        const testing = {
            type: 'bar',
            data: {
                labels: ['Africa', 'Asia', 'Europe', 'Latin America', 'North America'],
                datasets: [
                    {
                        label: 'Population (millions)',
                        backgroundColor: ['#3e95cd', '#8e5ea2', '#3cba9f', '#e8c3b9', '#c45850'],
                        data: [2478, 5267, 734, 784, 433]
                    }
                ]
            },
            options: {
                legend: { display: false },
                title: {
                    display: true,
                    text: 'Predicted world population (millions) in 2050'
                }
            }
        };

        return new Chart(document.getElementById('testingOne'), testing);
    }

    updateChart(): Chart {
        console.log('UpdateChart', this.testingOneChart);

        this.testingOneChart.data.datasets.forEach((dataset) => {
            console.log('dataset', dataset);
            console.log('dataset.labels', dataset['labels']);
            dataset.labels = ['Africa', 'Asia', 'Europe', 'Latin America', 'North America'];
            dataset.data = [this.magic(), this.magic(), this.magic(), this.magic(), this.magic()];
            dataset.backgroundColor = [this.colorsService.randomName(), this.colorsService.randomName(),
            this.colorsService.randomName(), this.colorsService.randomName(), this.colorsService.randomName()];
        });

        this.testingOneChart.update();
    }

    magic(): number {
        return Math.random() * (1000 - 100) + 100;
    }
}
