// Ngx Core imports
import * as Chart from 'chart.js';

// Application imports
import { ChartsEventEmitterService } from '../services/charts/charts-event-emitter.service';
import { ChartEntry } from './charts-implementation';

export interface IChartEntry {

    /** Deve criar um objeto ChartEntry válido
     * data: encapsulamento das propriedades inerentes àquela entry
     */
    // create({ data: any }): IChartEntry;

    /** Deve retornar a instância presente do ChartEntry */
    // get(): any;

    /** Deve printar o conteúdo de acordo */
    print(): void;

}

export interface IChartContent {
    data: any;

    addEntry(entry: IChartEntry);
    isEmpty(): boolean;
    print(): void;
}

export interface IChart {
    content: IChartContent;

    getDataLabels(): string[];
    getDataValues(): string[];
    makeEntry(key: string, value: string): IChartEntry;
    /** Popular todo o conteudo necessario do Chart */
    populateContent(ChartEntry): void;
    preCreationChecklist(elementId): boolean;
    createChart(): Chart;
    destroyChart(): void;
    onClick(evt: any, item: any): void;
    print(): void;
}

// ========= NEW STUFF ==========

/**
 * Agrupa um par de chave/valor.
 */
export interface IChartEntryNEW {
    key: any;
    value: any;
    make(key, value): IChartEntryNEW;
    getRounded(value: any): any;
    print();
}

/**
 * Contém uma coleção de IChartEntryNEW e métodos relacionados.
 */
export interface IChartContentNEW {
    addEntry(entry: IChartEntryNEW): void;

    setLabels(labels: any): any;
    getLabels(): any;
    getDataLabels(): any;
    getDataValues(): any;
    /**
     * Retorna conteudo para datasets
     */
    getData(): any;
    print(): void;
}

/**
 * Contém a representação visual de um ChartJS
 */
export interface IMetadataNEW {

    /**
     * Label externa.
     */
    labels: any;
    datasetLabel: any;
    datasetData: any;
    backgroundColor: string[];
    titleText: string;
    // core: any;

    onClick(evt: any, item: any): void;

    getMetadataCore(): any;
    assembleDataset(Components): any;
    updateDataset(Components): any;
    getLabels(): any;
    getDatasetLabel(): any;
    getDatasetData(): any;
    getBackgroundColor(): any;
    getTitleText(): any;

    print();
}

/**
 * Envelope ao redor do ChartJS, com métodos 'chainable'.
 */
export interface IChartNEW {

    /**
     * Contém os dados a serem apresentados.
     * @param content Contêiner para os dados.
     */
    setContent(content: IChartContentNEW): IChartNEW;

    /**
     * Seta o objeto que controla a representação visual do gráfico.
     * @param param0 Metadata Object
     */
    setMetaDataContent(metadata: IMetadataNEW): IChartNEW;


    // /**
    //  *
    //  * @param method Pass 'update' or 'destroy' for the handling of existing charts rendered.
    //  */
    // preRenderChecklist(method: string): IChartNEW;

    /**
     * Instancia a representação visual do gráfico na tela.
     * @param chartCanvasId Identificação do canvas na view
     */
    renderChart(chartCanvasId: string): Chart;

    /**
     * Atualiza os metadados do gráfico.
     * @param metadata
     */
    updateMetadata(metadata: IMetadataNEW): any;

    /**
     *  Atualiza os dados do gráfico e dispara as animações de acordo.
     * @param content
     * @param metadata
     */
    updateChart(content: IChartContentNEW, metadata?: IMetadataNEW): any;
}

/**
 * Tradutor do conteúdo bruto recebido da API para um IChartContentNEW
 */
export interface IChartParser {
    parse(responseData: any, sort: boolean): IChartContentNEW;
    sort(a: any, b: any, label?: string);
}

export interface IParserDataIdentifiers {
    // /**
    //  * Container para propriedades aninhadas.
    //  */
    // getAggregator(): any;
    // getLabel(): any;
    // getData(): any;

    /**
     * Retorna uma coleção de labels para uso ordenado na decodificação de respostas.
     */
    getLabels(): string[];
}

/**
 * Etiqueta 'Made in...'
 */
export interface IFactoryID {
    factoryID: string;
    set(factoryID: string);
}
