// Ngx Core imports
import * as Chart from 'chart.js';

// Application imports
import { ChartsEventEmitterService } from '../services/charts/charts-event-emitter.service';
import {
    IChart, IChartContent, IChartEntry,         // Old Imports
    IChartNEW, IChartContentNEW,                // New Imports
    IChartEntryNEW, IMetadataNEW
} from './chart-entities.model';
import { ColorsService } from '../services/utility/colors.service';

// ========= NEW STUFF ==========
export class ChartCommonEntry implements IChartEntryNEW {

    key: string;
    value: string;

    make(key: string, value: string) {
        if (!key && key == null || !value && value == null) {
            throw new Error('Exception: bad Key/Value!');
        }

        this.key = key;
        this.value = value;

        return this;
    }

    getRounded(value: string): string {
        // +this.value converte perfeitamente e não retorna erros,
        // pois +null retorna 0, não NaN como parseInt().
        // return (+this.value * 100).toFixed(1);
        return (+value * 100).toFixed(1);
    }

    print() {
        console.log('key::', JSON.stringify(this.key));
        console.log('value::', JSON.stringify(this.value));
    }

}

export class ChartMultiValueEntry implements IChartEntryNEW {

    key: string;
    value: string[];

    make(key: string, value: string[]) {
        if (!key && key == null || !value && value == null) {
            throw new Error('Exception: bad Key/Value!');
        }

        this.key = key;
        this.value = value.map(v => this.getRounded(v));

        return this;
    }

    getRounded(value: string): string {
        // +this.value converte perfeitamente e não retorna erros,
        // pois +null retorna 0, não NaN como parseInt().
        return (+value * 100).toFixed(1);
    }

    print() {
        console.log('key::', JSON.stringify(this.key));
        console.log('value::', JSON.stringify(this.value));
    }

}

export class ChartStackedEntry implements IChartEntryNEW {

    key: string;
    value: string[];

    make(key: string, value: string[]) {
        if (!key && key == null || !value && value == null) {
            throw new Error('Exception: bad Key/Value!');
        }

        this.key = key;
        this.value = value;

        return this;
    }

    getRounded(value: string): string {
        // +this.value converte perfeitamente e não retorna erros,
        // pois +null retorna 0, não NaN como parseInt().
        return (+value * 100).toFixed(1);
    }

    print() {
        console.log('key::', JSON.stringify(this.key));
        console.log('value::', JSON.stringify(this.value));
    }

}

export class ChartAggregateContent implements IChartContentNEW {

    public data = new Array<IChartEntry>();
    private labels = Array<string>(); // months
    private datasetLabels = Array<string>(); // labs
    private datasetValues = Array<string>(); // labValues

    addEntry(entry: IChartEntryNEW): void {

        if (this.data.indexOf(entry) !== 1) {
            this.data.push(entry);
        }
    }

    setLabels(labels: string[]) {
        this.labels = labels;
    }

    getLabels() {
        return this.labels;
    }

    getDataLabels() {
        throw new Error('Method not implemented.');
    }

    getDataValues() {
        throw new Error('Method not implemented.');
    }

    getData() {
        return this.data;
    }

    print(): void {
        console.log('ChartAggregateContent::data', JSON.stringify(this.data));
        console.log('ChartAggregateContent::labels', JSON.stringify(this.labels));
    }
}

export class ChartDailyContent implements IChartContentNEW {

    public data = new Array<IChartEntry>();
    private labels = Array<string>(); // months
    private datasetLabels = Array<string>(); // labs
    private datasetValues = Array<string>(); // labValues

    addEntry(entry: IChartEntryNEW): void {

        if (this.data.indexOf(entry) !== 1) {
            this.data.push(entry);

            // Aproveitamos esse momento para preenchermos essas propriedades q facilitam a
            // criação de nossos charts
            this.labels.push(entry.key);
            this.datasetValues.push(entry.getRounded(entry.value));
        }
    }

    setLabels(labels: string[]) {
        this.labels = labels;
    }

    getLabels() {
        return this.labels;
    }

    getDataLabels() {
        throw new Error('Method not implemented.');
    }

    getDataValues() {
        return this.datasetValues;
    }

    getData() {
        return this.data;
    }

    print(): void {
        console.log('ChartDailyContent::data', JSON.stringify(this.data));
        console.log('ChartDailyContent::labels', JSON.stringify(this.labels));
    }
}

export class ChartDoubleStackedContent implements IChartContentNEW {

    public data = new Array<IChartEntry>();
    private labels = Array<string>(); // months
    private datasetLabels = Array<string>(); // labs
    private datasetValues = Array<string>(); // labValues

    // 1 entry = label + data -> 1 dataset
    addEntry(entry: IChartEntryNEW): void {

        if (this.data.indexOf(entry) !== 1) {
            this.data.push(entry);
        }
    }

    setLabels(labels: string[]) {
        this.labels = labels;
    }

    getLabels() {
        return this.labels;
    }

    getDataLabels() {
        throw new Error('Method not implemented.');
    }

    getDataValues() {
        throw new Error('Method not implemented.');
    }

    getData() {
        return this.data;
    }

    print(): void {
        console.log('ChartAggregateContent::data', JSON.stringify(this.data));
        console.log('ChartAggregateContent::labels', JSON.stringify(this.labels));
    }
}

export class ChartCommonContent implements IChartContentNEW {

    public data = new Array<IChartEntry>();
    private dataLabels = Array<string>();
    private dataValues = Array<string>();

    addEntry(entry: IChartEntryNEW): void {
        if (this.data.indexOf(entry) !== 1) {
            this.data.push(entry);

            // Aproveitamos esse momento para preenchermos essas propriedades q facilitam a
            // criação de nossos charts
            this.dataLabels.push(entry.key);
            this.dataValues.push(entry.getRounded(entry.value));
        }
    }

    setLabels(labels: any) {
        throw new Error('Method not implemented.');
    }

    getLabels() {
        throw new Error('Method not implemented.');
    }

    getDataLabels() {
        return this.dataLabels;
    }

    getDataValues() {
        return this.dataValues;
    }

    getData() {
        return this.data;
    }

    print(): void {
        console.log('ChartCommonContent::', JSON.stringify(this.data));
    }
}

export class ChartAggregate implements IChartNEW {

    private content: IChartContentNEW;
    private metadata: IMetadataNEW;
    private _chart: Chart;

    setContent(content: IChartContentNEW): IChartNEW {
        console.log('ChartAggregate::setContent::starting:', JSON.stringify(content));
        this.content = content;

        return this;
    }

    setMetaDataContent(metadata: IMetadataNEW): IChartNEW {
        console.log('ChartAggregate::setMetaData::starting:');

        this.metadata = metadata;

        this.metadata.assembleDataset({
            aggregator: this.content.getLabels(),
            data: this.content.getData()
        });

        this.metadata.print();

        return this;
    }

    /**
     * Certifica que o gráfico pode ser renderizado corretamente.
     * @param strategy Passe 'update' ou 'destroy' para informar como tratar assets pré-existentes
     * @param chartCanvasId O elemento a ser verificado
     */
    private preRenderChecklist(strategy: string, chartCanvasId: string): boolean {
        console.log('preRenderChecklist::starting:', JSON.stringify(strategy + ' @' + chartCanvasId));

        if (!document.getElementById(chartCanvasId)) {
            console.error('Exception: Canvas ID not found.');
            return false;
        }

        if (!this._chart || this._chart == null) { return true; }

        let greenlight = false;
        if (strategy === 'destroy') {

            try {
                console.log('Attempting to destroy chart...');
                this._chart.destroy();
                greenlight = true;

            } catch (error) {
                throw new Error('Exception: Chart couldn\'t be destroyed.');
            }
        }

        return greenlight;
    }

    renderChart(chartCanvasId: string) {
        const greenLight = this.preRenderChecklist('destroy', chartCanvasId);
        if (!greenLight) { throw new Error('Exception: Chart couldn\'t be created.'); }

        console.log('ChartAggregate::renderChart::starting:', JSON.stringify(this.metadata.getMetadataCore()));

        const canvas: any = document.getElementById(chartCanvasId);
        const context: any = canvas.getContext('2d');

        this._chart = new Chart(context, this.metadata.getMetadataCore());

        return this._chart;
    }

    updateMetadata(metadata: IMetadataNEW) {
        this.setMetaDataContent(metadata);

        this.metadata.updateDataset(
            this.metadata.assembleDataset({
                aggregator: this.content.getLabels(),
                data: this.content.getData()
            }));

        // this._chart.data.datasets.forEach(dataset => {
        //     dataset.labels = this.metadata.datasetLabel;
        //     dataset.data = this.metadata.assembleDataset;
        // });
    }

    updateChart(content: IChartContentNEW, metadata?: IMetadataNEW) {
        throw new Error('Method not implemented');

        // this.setContent(content)
        //     .updateMetadata(metadata);

        // this._chart.update();
    }

}

export class ChartStacked implements IChartNEW {

    private content: IChartContentNEW;
    private metadata: IMetadataNEW;
    private _chart: Chart;

    setContent(content: IChartContentNEW): IChartNEW {
        console.log('ChartStacked::setContent::starting:', JSON.stringify(content));
        this.content = content;

        return this;
    }

    setMetaDataContent(metadata: IMetadataNEW): IChartNEW {
        console.log('ChartStacked::setMetaData::starting:');

        this.metadata = metadata;

        this.metadata.assembleDataset({
            aggregator: this.content.getLabels(),
            data: this.content.getData()
        });

        this.metadata.print();

        return this;
    }

    /**
     * Certifica que o gráfico pode ser renderizado corretamente.
     * @param strategy Passe 'update' ou 'destroy' para informar como tratar assets pré-existentes
     * @param chartCanvasId O elemento a ser verificado
     */
    private preRenderChecklist(strategy: string, chartCanvasId: string): boolean {
        console.log('preRenderChecklist::starting:', JSON.stringify(strategy + ' @' + chartCanvasId));

        if (!document.getElementById(chartCanvasId)) {
            console.error('Exception: Canvas ID not found.');
            return false;
        }

        if (!this._chart || this._chart == null) { return true; }

        let greenlight = false;
        if (strategy === 'destroy') {

            try {
                console.log('Attempting to destroy chart...');
                this._chart.destroy();
                greenlight = true;

            } catch (error) {
                throw new Error('Exception: Chart couldn\'t be destroyed.');
            }
        }

        return greenlight;
    }

    renderChart(chartCanvasId: string) {
        const greenLight = this.preRenderChecklist('destroy', chartCanvasId);
        if (!greenLight) { throw new Error('Exception: Chart couldn\'t be created.'); }

        console.log('ChartStacked::renderChart::starting:', JSON.stringify(this.metadata.getMetadataCore()));

        const canvas: any = document.getElementById(chartCanvasId);
        const context: any = canvas.getContext('2d');

        this._chart = new Chart(context, this.metadata.getMetadataCore());

        return this._chart;
    }

    updateMetadata(metadata: IMetadataNEW) {
        this.setMetaDataContent(metadata);

        this.metadata.updateDataset(
            this.metadata.assembleDataset({
                aggregator: this.content.getLabels(),
                data: this.content.getData()
            }));

        // this._chart.data.datasets.forEach(dataset => {
        //     dataset.labels = this.metadata.datasetLabel;
        //     dataset.data = this.metadata.assembleDataset;
        // });
    }

    updateChart(content: IChartContentNEW, metadata?: IMetadataNEW) {
        throw new Error('Method not implemented');

        // this.setContent(content)
        //     .updateMetadata(metadata);

        // this._chart.update();
    }

}

export class ChartDaily implements IChartNEW {

    private content: IChartContentNEW;
    private metadata: IMetadataNEW;
    private _chart: Chart;

    setContent(content: IChartContentNEW): IChartNEW {
        console.log('ChartDaily::setContent::starting:', JSON.stringify(content));
        this.content = content;

        return this;
    }

    setMetaDataContent(metadata: IMetadataNEW): IChartNEW {
        console.log('ChartDaily::setMetaData::starting:');

        this.metadata = metadata;

        this.metadata.assembleDataset({
            aggregator: this.content.getLabels(),
            data: this.content.getDataValues()
        });

        this.metadata.print();

        return this;
    }

    /**
     * Certifica que o gráfico pode ser renderizado corretamente.
     * @param strategy Passe 'update' ou 'destroy' para informar como tratar assets pré-existentes
     * @param chartCanvasId O elemento a ser verificado
     */
    private preRenderChecklist(strategy: string, chartCanvasId: string): boolean {
        console.log('preRenderChecklist::starting:', JSON.stringify(strategy + ' @' + chartCanvasId));

        if (!document.getElementById(chartCanvasId)) {
            console.error('Exception: Canvas ID not found.');
            return false;
        }

        if (!this._chart || this._chart == null) { return true; }

        let greenlight = false;
        if (strategy === 'destroy') {

            try {
                console.log('Attempting to destroy chart...');
                this._chart.destroy();
                greenlight = true;

            } catch (error) {
                throw new Error('Exception: Chart couldn\'t be destroyed.');
            }
        }

        return greenlight;
    }

    renderChart(chartCanvasId: string) {
        const greenLight = this.preRenderChecklist('destroy', chartCanvasId);
        if (!greenLight) { throw new Error('Exception: Chart couldn\'t be created.'); }

        console.log('ChartDaily::renderChart::starting:', JSON.stringify(this.metadata.getMetadataCore()));

        const canvas: any = document.getElementById(chartCanvasId);
        const context: any = canvas.getContext('2d');

        this._chart = new Chart(context, this.metadata.getMetadataCore());

        return this._chart;
    }

    updateMetadata(metadata: IMetadataNEW) {
        this.setMetaDataContent(metadata);

        this.metadata.updateDataset(
            this.metadata.assembleDataset({
                aggregator: this.content.getLabels(),
                data: this.content.getData()
            }));

        // this._chart.data.datasets.forEach(dataset => {
        //     dataset.labels = this.metadata.datasetLabel;
        //     dataset.data = this.metadata.assembleDataset;
        // });
    }

    updateChart(content: IChartContentNEW, metadata?: IMetadataNEW) {
        throw new Error('Method not implemented');

        // this.setContent(content)
        //     .updateMetadata(metadata);

        // this._chart.update();
    }

}



/**
 *  O tipo mais básico de gráfico, usa chave/valor
 */
export class ChartCommon implements IChartNEW {

    // Returning 'this' so it remains chainable

    private content: IChartContentNEW;
    private metadata: IMetadataNEW;
    private _chart: Chart;

    setContent(content: IChartContentNEW): IChartNEW {
        console.log('ChartCommon::setContent::starting:', JSON.stringify(content));
        this.content = content;

        return this;
    }

    setMetaDataContent(metadata: IMetadataNEW): IChartNEW {
        // console.log('ChartCommon::setMetaData::starting:', JSON.stringify(metadata));
        console.log('ChartCommon::setMetaData::starting:');

        this.metadata = metadata;
        // Forma para acessar as propriedades do ChartJS, intermediada pela classe MetaData
        // que necessitem de estado atualizado para aferição de conteudo
        this.metadata.datasetLabel = this.content.getDataLabels();
        this.metadata.datasetData = this.content.getDataValues();

        // this.metadata.print();

        return this;
    }

    updateMetadata(metadata: IMetadataNEW) {
        this.setMetaDataContent(metadata);

        this._chart.data.datasets.forEach(dataset => {
            dataset.labels = this.metadata.datasetLabel;
            dataset.data = this.metadata.datasetData;
        });
    }


    /**
     * Certifica que o gráfico pode ser renderizado corretamente.
     * @param strategy Passe 'update' ou 'destroy' para informar como tratar assets pré-existentes
     * @param chartCanvasId O elemento a ser verificado
     */
    private preRenderChecklist(strategy: string, chartCanvasId: string): boolean {
        console.log('preRenderChecklist::starting:', JSON.stringify(strategy + ' @' + chartCanvasId));

        if (!document.getElementById(chartCanvasId)) {
            console.error('Exception: Canvas ID not found.');
            return false;
        }

        if (!this._chart || this._chart == null) { return true; }

        let greenlight = false;
        if (strategy === 'destroy') {

            try {
                console.log('Attempting to destroy chart...');
                this._chart.destroy();
                greenlight = true;

            } catch (error) {
                throw new Error('Exception: Chart couldn\'t be destroyed.');
            }
        }

        return greenlight;
    }

    renderChart(chartCanvasId: string): Chart {

        const greenLight = this.preRenderChecklist('destroy', chartCanvasId);
        if (!greenLight) { throw new Error('Exception: Chart couldn\'t be created.'); }

        console.log('ChartCommon::renderChart::starting:', JSON.stringify(this.metadata.getMetadataCore()));

        // this.metadata.print();

        const canvas: any = document.getElementById(chartCanvasId);
        const context: any = canvas.getContext('2d');

        this._chart = new Chart(context, this.metadata.getMetadataCore());

        return this._chart;
    }

    updateChart(content: IChartContentNEW, metadata: IMetadataNEW) {

        this.setContent(content)
            // .setMetaDataContent(metadata)
            .updateMetadata(metadata);

        this._chart.update();
    }

}


// ======== /NEW STUFF ==========


// TODO:
//  Acertar nomes (DDD) - especialmente da árvore p/ graf. de linhas

export class ChartLineEntry implements IChartEntry {

    constructor(public line: string, public values: string[]) { }

    print(): void {
        console.log('ChartLineEntry::line::', JSON.stringify(this.line));
        console.log('ChartLineEntry::values::', JSON.stringify(this.values));
    }
}

export class ChartEntry implements IChartEntry {

    constructor(public key: string, public value: string) { }

    print(): void {
        console.log('ChartEntry::key', JSON.stringify(this.key));
        console.log('ChartEntry::value', JSON.stringify(this.value));
    }
}

/** Default implementation for ChartContent */
export class ChartContent implements IChartContent {

    public data = new Array<IChartEntry>();

    addEntry(entry: IChartEntry) {
        if (this.data.indexOf(entry) !== 1) {
            this.data.push(entry);
        }
    }

    isEmpty(): boolean {
        return this.data.length <= 0;
    }

    print(): void {
        this.data.forEach(element => {
            element.print();
            // console.log(this.lblKey, JSON.stringify(element.key));
            // console.log(this.lblValue, JSON.stringify(element.value));
        });
    }

}

export class ChartLabelTexts {
    constructor(public chartTitleText: string, public chartLabel: string, public chartLegend: string, public chartScaleLabel: string) { }
}

export class ChartHtmlInfo {

    constructor(public elementId: string) { }
}

/** Default implementation for ChartEntity */
export class ChartEntity implements IChart {

    protected _chart: Chart;

    /** Para propósitos de filtros */
    protected context: any;
    protected canvas: any;
    protected chartEventID = '';

    public content: IChartContent;

    protected labels = new Array<string>();
    protected data = new Array<string>();

    public lblKey: string;
    public lblValue: string;

    public chartLabelTexts: ChartLabelTexts;
    public chartHtmlInfo: ChartHtmlInfo;

    constructor(
        chartHtmlInfo: ChartHtmlInfo,
        chartLabelTexts: ChartLabelTexts,
        content: IChartContent,
        private eventEmitter: ChartsEventEmitterService,
        chartEventID?: string) {

        this.chartHtmlInfo = chartHtmlInfo;
        this.chartEventID = chartEventID;
        this.content = content;
        this.chartLabelTexts = chartLabelTexts;
    }


    getDataLabels(): string[] {
        return this.labels;
    }

    getDataValues(): string[] {
        return this.data;
    }

    // Methods that will get repeated a lot
    populateContent(entry): void {
        this.labels.push((entry.key as string).toUpperCase());
        this.data.push((entry.value));

        this.content.addEntry(entry);
    }

    /** Padrão é trazer como percentual */
    makeEntry(key, value): IChartEntry {

        // Código temporário - esse método deve retornar objeto não nulo
        if (!key || (!value && value !== 0)) {
            // if (!key || !value) {
            console.log('Empty/null data field found::' + JSON.stringify([key, value]));
            return;
        }

        return new ChartEntry(key, ((value * 100).toFixed(1)));
    }

    // Bug: Ghost Charts
    preCreationChecklist(elementId): boolean {
        try {
            if (!document.getElementById(elementId)) {
                // console.log('GHOST CHART ' + elementId + ' GHOSTBUSTED');
                return false;
            }
        } catch (error) {
            // console.log('GHOST CHART ' + elementId + '  GHOSTBUSTED', error);
            return false;
        }

        if (this._chart || this._chart != null) { this.destroyChart(); }

        return true;
    }

    createChart() {
        throw new Error('Method not implemented.');
    }

    destroyChart() {
        this._chart.destroy();
    }

    // Events
    onClick(evt: any, item: any) {
        this.eventEmitter.emitClick(item, this.chartEventID);
    }

    print() {
        console.log('ChartDistributor::', JSON.stringify(this.content));
        this.content.print();
        console.log('');
    }
}


export class ChartDistributor extends ChartEntity implements IChart {

    constructor(
        chartHtmlInfo: ChartHtmlInfo,
        chartLabelTexts: ChartLabelTexts,
        eventEmitter: ChartsEventEmitterService,
        chartEventID?: string) {
        super(chartHtmlInfo, chartLabelTexts, new ChartContent(), eventEmitter);

        this.chartEventID = chartEventID || 'DIST';
        // this.elementId = 'distributors'; // TROCARRRRRRRRRRRRRRRRRRRRRRRR
        this.lblKey = 'DISTRIBUIDOR';
        this.lblValue = 'RUPTURA';
    }

    createChart(): Chart {

        if (this.preCreationChecklist(this.chartHtmlInfo.elementId) === false) {
            console.log('Chart could not be created:route changed.');
            return;
        };

        this.canvas = document.getElementById(this.chartHtmlInfo.elementId);
        this.context = this.canvas.getContext('2d');

        this._chart = new Chart(this.context, {
            type: 'horizontalBar',
            data: {
                labels: this.getDataLabels(),
                datasets: [{
                    label: this.chartLabelTexts.chartLabel,
                    data: this.getDataValues(),
                    backgroundColor: [
                        '#2a5c5b', '#316362', '#376968', '#3e706f',
                        '#457776', '#487a79', '#4d7f7e', '#518382',
                        '#568887', '#5c8e8d', '#5e908f', '#649695',
                        '#6c9e9d', '#78aaa9', '#81b3b2']
                }]
            },
            options: {
                /* Evento que dispara o filtro para a atualização de bases */
                'onClick': (evt, item) => {
                    this.onClick(evt, item);
                },
                tooltips: {
                    enabled: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: 'white',
                            fontSize: 10,
                            stepSize: 0,
                            beginAtZero: true,
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        display: false,
                        ticks: {
                            fontColor: 'white',
                            fontSize: 10,
                            stepSize: 0,
                            beginAtZero: true
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                },
                animation: {
                    onProgress: function () {
                        const ctx = this.chart.ctx;
                        ctx.font = Chart.helpers.fontString(
                            Chart.defaults.global.defaultFontFamily,
                            'normal',
                            Chart.defaults.global.defaultFontFamily);
                        ctx.fillStyle = 'white';
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'center';

                        this.data.datasets.forEach(function (dataset) {
                            for (let i = 0; i < dataset.data.length; i++) {
                                for (const key in dataset._meta) {

                                    if (dataset._meta.hasOwnProperty(key)) {
                                        const element = dataset._meta[key];
                                        const model = dataset._meta[key].data[i]._model;
                                        ctx.fillText(dataset.data[i] + '%', model.x + 22, model.y + 5);
                                    }
                                }
                            }
                        });
                    }
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 40,
                        top: 0,
                        bottom: 0
                    }
                },
                legend: {
                    display: false
                },
                title: {
                    fontColor: 'white',
                    display: false,
                    text: this.chartLabelTexts.chartTitleText
                },
                responsive: true,
                display: true
            }
        });

        return this._chart;
    }
}

export class ChartSalesStatus extends ChartEntity implements IChart {

    constructor(
        chartHtmlInfo: ChartHtmlInfo,
        chartLabelTexts: ChartLabelTexts,
        eventEmitter: ChartsEventEmitterService,
        chartEventID?: string) {
        super(chartHtmlInfo, chartLabelTexts, new ChartContent(), eventEmitter);

        this.chartEventID = chartEventID || 'SALE';
        // this.elementId = 'salesstatus';
        this.lblKey = 'STATUS VENDA';
        this.lblValue = 'RUPTURA';
    }

    createChart(): Chart {
        if (this.preCreationChecklist(this.chartHtmlInfo.elementId) === false) {
            console.log('Chart could not be created:route changed.');
            return;
        };

        this._chart = new Chart(document.getElementById(this.chartHtmlInfo.elementId), {
            type: 'horizontalBar',
            data: {
                labels: this.getDataLabels(),
                datasets: [
                    {
                        label: this.chartLabelTexts.chartLabel,
                        data: this.getDataValues(),
                        backgroundColor: ['#b94a0f', '#c04e11',
                            '#c65215', '#cb5a1f', '#d25e21', '#d76224',
                            '#da672b', '#e06d31', '#e3763d', '#e3763d'],
                    }
                ]
            },
            options: {
                // 'onClick': (evt, item) => {
                //     this.onClick(evt, item);
                // },
                layout: {
                    padding: {
                        left: 0,
                        right: 30,
                        top: 0,
                        bottom: 0
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: 'white',
                            fontSize: 10,
                            stepSize: 0,
                            beginAtZero: true,
                            barThickness: 10
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        display: false,
                        ticks: {
                            fontColor: 'white',
                            fontSize: 10,
                            stepSize: 0,
                            beginAtZero: true
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                },
                legend: { display: false },
                title: {
                    display: false,
                    fontColor: 'white',
                    text: this.chartLabelTexts.chartTitleText
                }
            }
        });

        return this._chart;

    }
}

export class ChartUF extends ChartEntity implements IChart {

    constructor(
        chartHtmlInfo: ChartHtmlInfo,
        chartLabelTexts: ChartLabelTexts,
        eventEmitter: ChartsEventEmitterService,
        chartEventID?: string) {
        super(chartHtmlInfo, chartLabelTexts, new ChartContent(), eventEmitter);

        this.chartEventID = chartEventID || 'UF';
        this.lblKey = 'UF';
        this.lblValue = 'RUPTURA';
    }


    createChart() {
        if (this.preCreationChecklist(this.chartHtmlInfo.elementId) === false) {
            console.log('Chart could not be created:route changed.');
            return;
        };

        this.canvas = document.getElementById(this.chartHtmlInfo.elementId);
        this.context = this.canvas.getContext('2d');


        this._chart = new Chart(this.context, {
            type: 'bar',
            data: {
                labels: this.getDataLabels(),
                datasets: [{
                    label: 'Percentage',
                    data: this.getDataValues(),
                    backgroundColor: [
                        '#2c5458', '#348992', '#2c5458', '#348992',
                        '#2c5458', '#348992', '#2c5458', '#348992',
                        '#2c5458', '#348992', '#2c5458', '#348992',
                        '#2c5458', '#348992', '#2c5458', '#348992',
                        '#2c5458', '#348992', '#2c5458', '#348992',
                        '#2c5458', '#348992', '#2c5458', '#348992',
                        '#2c5458', '#348992', '#2c5458', '#348992',
                        '#2c5458', '#348992', '#2c5458', '#348992',
                    ]
                }]
            },
            options: {
                /* Evento que dispara o filtro para a atualização de bases */
                'onClick': (evt, item) => {
                    this.onClick(evt, item);
                },
                tooltips: {
                    enabled: true
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 10,
                        bottom: 0
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: 'white',
                            fontSize: 8,
                            beginAtZero: false
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            fontColor: 'white',
                            fontSize: 8,
                            stepSize: 0,
                            beginAtZero: true
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                },
                legend: { display: false },
                title: {
                    display: false,
                    fontColor: 'white',
                    text: 'By State - Last Month'
                }
            }
        });

        return this._chart;

    }
}

/** Named Nested Line Chart */
export class ChartBU extends ChartEntity implements IChart {

    public lblLine: Array<string>;
    protected dataset: any[];

    constructor(
        chartHtmlInfo: ChartHtmlInfo,
        chartLabelTexts: ChartLabelTexts,
        eventEmitter: ChartsEventEmitterService,
        private colorsService: ColorsService,
        chartEventID?: string) {

        super(chartHtmlInfo, chartLabelTexts, new ChartContent(), eventEmitter);

        this.chartEventID = chartEventID || 'BU';
        // this.elementId = 'bu';

        this.lblKey = 'Name';
        this.lblValue = 'Value';
        this.lblLine = new Array<string>();

        this.dataset = [];
    }

    getDataValues() {
        return this.dataset;
    }

    // Vai receber um LAB e uma lista de valores (ordem importa)
    makeEntry(line, data): IChartEntry {
        return new ChartLineEntry(line, data);
    }

    // lines = string[]
    // data = IChartEntry[]
    populateContent({ lines, data }): void {
        this.labels = lines;

        // Codigo Temporario Seleção de Cores
        const colors = this.colorsService.fixColor();
        let count = 0;

        for (const entry in data) {
            if (data.hasOwnProperty(entry)) {
                const set = data[entry];

                const dataEntry = {
                    data: set.values,
                    label: set.line,
                    // borderColor: this.colorsService.randomName(),
                    borderColor: colors[count],
                    borderWidth: 1.2,
                    fill: false,
                };

                this.dataset.push(dataEntry);

                // Codigo Temporario Seleção de Cores
                count++;
            }
        }
    }


    createChart() {
        if (this.preCreationChecklist(this.chartHtmlInfo.elementId) === false) {
            console.log('Chart could not be created:route changed.');
            return;
        };

        this.canvas = document.getElementById(this.chartHtmlInfo.elementId);
        this.context = this.canvas.getContext('2d');

        this._chart = new Chart(this.context, {
            type: 'line',
            data: {
                labels: this.labels,
                datasets: this.dataset
            },
            options: {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: 'white',
                            fontSize: 8,

                            beginAtZero: false
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            fontColor: 'white',
                            fontSize: 8,
                            stepSize: 10,
                            beginAtZero: false
                        },
                        gridLines: {
                            color: 'grey'
                        }
                    }]
                },
                title: {
                    labels: { fontColor: '#ffffff' },
                    display: false,
                    fontColor: 'white',
                    text: this.chartLabelTexts.chartTitleText
                },
                legend: {
                    labels: {
                        boxWidth: 8,
                        fontSize: 8,
                        fontColor: 'white'
                    }
                },
            },
        });

        return this._chart;
    }

    print() {
        console.log('ChartBU::');
        console.log('lines', JSON.stringify(this.getDataLabels()));
        console.log('dataset', JSON.stringify(this.getDataValues()));
        console.log('');
    }

}

export class ChartLandingPage extends ChartEntity implements IChart {

    constructor(
        chartHtmlInfo: ChartHtmlInfo,
        graphEventID,
        lblKey,
        lblValue,
        chartLabelTexts: ChartLabelTexts,
        eventEmitter: ChartsEventEmitterService) {

        super(chartHtmlInfo, chartLabelTexts, new ChartContent(), eventEmitter);

        this.chartEventID = graphEventID;
        this.lblKey = lblKey;
        this.lblValue = lblValue;
    }

    createChart(): Chart {
        if (this.preCreationChecklist(this.chartHtmlInfo.elementId) === false) {
            console.log('Chart could not be created:route changed.');
            return;
        };

        this.canvas = document.getElementById(this.chartHtmlInfo.elementId);
        this.context = this.canvas.getContext('2d');

        this._chart = new Chart(this.context, {
            type: 'bar',
            data: {
                labels: this.getDataLabels(),
                datasets: [{
                    label: this.chartLabelTexts.chartLabel,
                    data: this.getDataValues(),
                    backgroundColor: ['#0d5394', '#2771b6', '#4995dc', '#64b3fc', '#1f51b7', '#940d3b', '#1f51b7', '#940d3b'],
                }]
            },
            options: {
                /* Evento que dispara o filtro para a atualização de bases */
                'onClick': (evt, item) => {
                    this.onClick(evt, item);
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: 'white',
                            beginAtZero: true,
                            fontSize: 12,
                        },
                        scaleLabel: {
                            display: true,
                            labelString: this.chartLabelTexts.chartScaleLabel,
                            fontColor: 'white',
                            fontSize: 12,
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            fontColor: 'white',
                            fontSize: 12,
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                },
                animation: {
                    onProgress: function () {
                        const ctx = this.chart.ctx;
                        ctx.font = Chart.helpers.fontString(
                            Chart.defaults.global.defaultFontFamily,
                            'normal',
                            Chart.defaults.global.defaultFontFamily);
                        ctx.fillStyle = 'white';
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'center';

                        this.data.datasets.forEach(function (dataset) {
                            for (let i = 0; i < dataset.data.length; i++) {
                                for (const key in dataset._meta) {

                                    if (dataset._meta.hasOwnProperty(key)) {
                                        const element = dataset._meta[key];
                                        const model = dataset._meta[key].data[i]._model;
                                        ctx.fillText(dataset.data[i] + '%', model.x + 0, model.y + -10);
                                    }
                                }
                            }
                        });
                    }
                },
                layout: {
                    padding: {
                        left: 25,
                        right: 0,
                        top: 30,
                        bottom: 0
                    }
                },
                legend: {
                    display: false
                },
                title: {
                    text: this.chartLabelTexts.chartTitleText,
                    display: false,
                },
                tooltips: {
                    enabled: false
                },
                responsive: true,
                display: true
            }
        });

        return this._chart;
    }

    onClick(evt, item): void {
        super.onClick(evt, item);
    }


}

