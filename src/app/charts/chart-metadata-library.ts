import { FactoryMapper } from '../settings/factory.mapper';
import * as Chart from 'chart.js';
import { IMetadataNEW, IChartEntryNEW } from './chart-entities.model';
import { ColorsService } from '../services/utility/colors.service';

export class MetadataDistributor implements IMetadataNEW {

    labels: string[];
    datasetLabel: string;
    datasetData: string[];
    backgroundColor = [
        '#ffffff', '#cccccc', '#ffffff', '#cccccc',
        '#ffffff', '#cccccc', '#ffffff', '#cccccc',
        '#ffffff', '#cccccc', '#ffffff', '#cccccc',
        '#ffffff', '#cccccc', '#ffffff', '#cccccc'];

    titleText: string;

    constructor() {
        this.titleText = 'Main Customers - Last Month';
        this.datasetLabel = 'Percentage';
    }

    // onProgress() { }

    onClick(evt: any, item: any): void {
        console.log('onClick', item);
    }

    getMetadataCore() {
        return {
            type: 'horizontalBar',
            data: {
                labels: this.getLabels(),
                datasets: [{
                    label: this.getDatasetLabel(),
                    data: this.getDatasetData(),
                    backgroundColor: this.getBackgroundColor()
                }]
            },
            options: {
                /* Evento que dispara o filtro para a atualização de bases */
                'onClick': (evt, item) => {
                    this.onClick(evt, item);
                },
                tooltips: {
                    enabled: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: 'white',
                            fontSize: 10,
                            stepSize: 0,
                            beginAtZero: true,
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        display: false,
                        ticks: {
                            fontColor: 'white',
                            fontSize: 10,
                            stepSize: 0,
                            beginAtZero: true
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                },
                animation: {
                    onProgress: function () {
                        const ctx = this.chart.ctx;
                        ctx.font = Chart.helpers.fontString(
                            Chart.defaults.global.defaultFontFamily,
                            'normal',
                            Chart.defaults.global.defaultFontFamily);
                        ctx.fillStyle = 'white';
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'center';

                        this.data.datasets.forEach(function (dataset) {
                            for (let i = 0; i < dataset.data.length; i++) {
                                for (const key in dataset._meta) {

                                    if (dataset._meta.hasOwnProperty(key)) {
                                        const element = dataset._meta[key];
                                        const model = dataset._meta[key].data[i]._model;
                                        ctx.fillText(dataset.data[i] + '%', model.x + 22, model.y + 5);
                                    }
                                }
                            }


                        });
                    }
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 40,
                        top: 0,
                        bottom: 0
                    }
                },
                legend: {
                    display: false
                },
                title: {
                    fontColor: 'white',
                    display: false,
                    text: this.getTitleText()
                },
                responsive: true,
                display: true
            }
        };
    }


    getLabels() {
        return this.datasetLabel;
    }
    getDatasetLabel() {
        return this.labels;
    }
    getDatasetData() {
        return this.datasetData;
    }
    getBackgroundColor() {
        return this.backgroundColor;
    }
    getTitleText() {
        return this.titleText;
    }

    assembleDataset(Components: any) {
        throw new Error('Method not implemented.');
    }

    updateDataset(Components: any) {
        throw new Error('Method not implemented.');
    }

    public print() {
        console.log('metadata::getMetadataCore::', JSON.stringify(this.getMetadataCore()));
        console.log('metadata::this.labels::', JSON.stringify(this.labels));
        console.log('metadata::this.datasetLabel::', JSON.stringify(this.datasetLabel));
        console.log('metadata::this.datasetData::', JSON.stringify(this.datasetData));
        console.log('metadata::this.backgroundColor::', JSON.stringify(this.backgroundColor));
    }

}

export class MetadataHorizontal implements IMetadataNEW {

    labels: string[];
    datasetLabel: string;
    datasetData: string[];
    backgroundColor = [
        '#4daddf', '#49a2d0', '#459ac6', '#418fb8',
        '#3887b0', '#3783aa', '#347fa5', '#32789c'];

    titleText: string;

    constructor() {
        this.titleText = 'titleText';
        this.datasetLabel = 'Percentage';
    }

    // onProgress() { }

    onClick(evt: any, item: any): void {
        console.log('onClick', item);
    }

    getMetadataCore() {
        return {
            type: 'horizontalBar',
            data: {
                labels: this.getLabels(),
                datasets: [{
                    label: this.getDatasetLabel(),
                    data: this.getDatasetData(),
                    backgroundColor: this.getBackgroundColor()
                }]
            },
            options: {
                /* Evento que dispara o filtro para a atualização de bases */
                'onClick': (evt, item) => {
                    this.onClick(evt, item);
                },
                tooltips: {
                    enabled: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: 'white',
                            fontSize: 10,
                            stepSize: 0,
                            beginAtZero: true,
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        display: false,
                        ticks: {
                            fontColor: 'white',
                            fontSize: 10,
                            stepSize: 0,
                            beginAtZero: true
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                },
                animation: {
                    onProgress: function () {
                        const ctx = this.chart.ctx;
                        ctx.font = Chart.helpers.fontString(
                            Chart.defaults.global.defaultFontFamily,
                            'normal',
                            Chart.defaults.global.defaultFontFamily);
                        ctx.fillStyle = 'white';
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'center';

                        this.data.datasets.forEach(function (dataset) {
                            for (let i = 0; i < dataset.data.length; i++) {
                                for (const key in dataset._meta) {

                                    if (dataset._meta.hasOwnProperty(key)) {
                                        const element = dataset._meta[key];
                                        const model = dataset._meta[key].data[i]._model;
                                        ctx.fillText(dataset.data[i] + '%', model.x + 22, model.y + 5);
                                    }
                                }
                            }


                        });
                    }
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 40,
                        top: 0,
                        bottom: 0
                    }
                },
                legend: {
                    display: false
                },
                title: {
                    fontColor: 'white',
                    display: false,
                    text: this.getTitleText()
                },
                responsive: true,
                display: true
            }
        };
    }


    getLabels() {
        return this.datasetLabel;
    }
    getDatasetLabel() {
        return this.labels;
    }
    getDatasetData() {
        return this.datasetData;
    }
    getBackgroundColor() {
        return this.backgroundColor;
    }
    getTitleText() {
        return this.titleText;
    }

    assembleDataset(Components: any) {
        throw new Error('Method not implemented.');
    }

    updateDataset(Components: any) {
        throw new Error('Method not implemented.');
    }

    public print() {
        console.log('metadata::getMetadataCore::', JSON.stringify(this.getMetadataCore()));
        console.log('metadata::this.labels::', JSON.stringify(this.labels));
        console.log('metadata::this.datasetLabel::', JSON.stringify(this.datasetLabel));
        console.log('metadata::this.datasetData::', JSON.stringify(this.datasetData));
        console.log('metadata::this.backgroundColor::', JSON.stringify(this.backgroundColor));
    }

}


export class MetadataSalesStatus implements IMetadataNEW {

    labels: string[];
    datasetLabel: string;
    datasetData: string[];
    backgroundColor = [
        '#ffffff', '#cccccc', '#ffffff', '#cccccc',
        '#ffffff', '#cccccc', '#ffffff', '#cccccc',
        '#ffffff', '#cccccc', '#ffffff', '#cccccc',
        '#ffffff', '#cccccc', '#ffffff', '#cccccc'];

    titleText: string;

    constructor() {
        this.titleText = 'Main Customers - Last Month';
        this.datasetLabel = 'Percentage';
    }

    // onProgress() { }

    onClick(evt: any, item: any): void {
        console.log('onClick', item);
    }

    getMetadataCore() {
        return {

            type: 'horizontalBar',
            data: {
                labels: this.getLabels(),
                datasets: [
                    {
                        label: this.getDatasetLabel(),
                        data: this.getDatasetData(),
                        backgroundColor: ['#b94a0f', '#c04e11',
                            '#c65215', '#cb5a1f', '#d25e21', '#d76224',
                            '#da672b', '#e06d31', '#e3763d', '#e3763d'],
                    }
                ]
            },
            options: {
                // 'onClick': (evt, item) => {
                //     this.onClick(evt, item);
                // },
                layout: {
                    padding: {
                        left: 0,
                        right: 30,
                        top: 0,
                        bottom: 0
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: 'white',
                            fontSize: 10,
                            stepSize: 0,
                            beginAtZero: true,
                            barThickness: 10
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        display: false,
                        ticks: {
                            fontColor: 'white',
                            fontSize: 10,
                            stepSize: 0,
                            beginAtZero: true
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                },
                legend: { display: false },
                title: {
                    display: false,
                    fontColor: 'white',
                    text: this.getTitleText()
                }
            }

        };
    }


    getLabels() {
        return this.datasetLabel;
    }
    getDatasetLabel() {
        return this.labels;
    }
    getDatasetData() {
        return this.datasetData;
    }
    getBackgroundColor() {
        return this.backgroundColor;
    }
    getTitleText() {
        return this.titleText;
    }

    assembleDataset(Components: any) {
        throw new Error('Method not implemented.');
    }

    updateDataset(Components: any) {
        throw new Error('Method not implemented.');
    }

    public print() {
        console.log('metadata::getMetadataCore::', JSON.stringify(this.getMetadataCore()));
        console.log('metadata::this.labels::', JSON.stringify(this.labels));
        console.log('metadata::this.datasetLabel::', JSON.stringify(this.datasetLabel));
        console.log('metadata::this.datasetData::', JSON.stringify(this.datasetData));
        console.log('metadata::this.backgroundColor::', JSON.stringify(this.backgroundColor));
    }

}

export class MetadataMonthly implements IMetadataNEW {

    labels: string[];
    datasetLabel: string;
    datasetData: string[];
    backgroundColor: string[];
    titleText: string;

    dataset: Array<any>;

    constructor(private colorsService: ColorsService) {
        this.titleText = 'TITLE TEXT';
        this.datasetLabel = 'PERCENTAGE';
        this.dataset = new Array<any>();
    }

    onClick(evt: any, item: any): void {
        console.log('onClick', item);
    }

    getMetadataCore() {
        return {
            type: 'line',
            data: {
                labels: this.getLabels(),
                datasets: this.dataset
            },
            options: {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: 'white',
                            fontSize: 8,
                            beginAtZero: false
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            fontColor: 'white',
                            fontSize: 8,
                            stepSize: 10,
                            beginAtZero: false
                        },
                        gridLines: {
                            color: 'grey'
                        }
                    }]
                },
                title: {
                    labels: { fontColor: '#ffffff' },
                    display: false,
                    fontColor: 'white',
                    text: this.getTitleText()
                },
                legend: {
                    labels: {
                        boxWidth: 8,
                        fontSize: 8,
                        fontColor: 'white'
                    }
                },
            },
        };
    }

    getLabels() {
        return this.labels;
    }

    getDatasetLabel() {
        return this.labels;
    }

    getDatasetData() {
        return this.datasetData;
    }

    // TODO: Colors.
    assembleDataset({ aggregator, data }): any {

        // Setando as labels externas
        this.labels = aggregator;
        // Codigo Temporario Seleção de Cores
        const colors = this.colorsService.fixColor();
        let count = 0;

        for (const entry in data) {
            if (data.hasOwnProperty(entry)) {
                const set = data[entry];

                const dataEntry = {
                    data: set.value,
                    label: set.key,
                    borderColor: colors[count],
                    borderWidth: 1.2,
                    fill: false,
                };

                this.dataset.push(dataEntry);
                count++;
            }
        }

        return this.dataset;
    }

    updateDataset({ dataset }) {
        this.dataset = dataset;
    }

    getBackgroundColor() {
        return this.backgroundColor;
    }

    getTitleText() {
        return this.titleText;
    }

    print() {
        console.log('metadata::getMetadataCore::', JSON.stringify(this.getMetadataCore()));
        console.log('metadata::this.labels::', JSON.stringify(this.labels));
        console.log('metadata::this.datasetLabel::', JSON.stringify(this.datasetLabel));
        console.log('metadata::this.datasetData::', JSON.stringify(this.datasetData));
        console.log('metadata::this.backgroundColor::', JSON.stringify(this.backgroundColor));
    }
}

export class MetadataStacked implements IMetadataNEW {

    labels: string[];
    datasetLabel: string;
    datasetData: string[];
    backgroundColor: string[];
    titleText: string;

    dataset: Array<any>;

    constructor(private colorsService: ColorsService) {
        this.titleText = 'TITLE TEXT';
        this.datasetLabel = 'PERCENTAGE';
        this.dataset = new Array<any>();
    }

    onClick(evt: any, item: any): void {
        console.log('onClick', item);
    }

    getMetadataCore() {
        return {
            type: 'bar',
            data: {
                labels: this.getLabels(),
                datasets: this.dataset
            },
            options: {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: 'white',
                            fontSize: 8,
                            beginAtZero: false
                        },
                        gridLines: {
                            display: false
                        },
                        stacked: true
                    }],
                    xAxes: [{
                        ticks: {
                            fontColor: 'white',
                            fontSize: 8,
                            stepSize: 10,
                            beginAtZero: false
                        },

                        stacked: true
                    }]
                },
                title: {
                    labels: { fontColor: '#ffffff' },
                    display: false,
                    fontColor: 'white',
                    text: this.getTitleText()
                },
                legend: {
                    labels: {
                        boxWidth: 8,
                        fontSize: 8,
                        fontColor: 'white'
                    }
                },
            },
        };
    }

    getLabels() {
        return this.labels;
    }

    getDatasetLabel() {
        return this.labels;
    }

    getDatasetData() {
        return this.datasetData;
    }

    // TODO: Colors.
    assembleDataset({ aggregator, data }): any {

        // Setando as labels externas
        this.labels = aggregator;
        // Codigo Temporario Seleção de Cores
        const colors = this.colorsService.fixColorBarras();
        let count = 0;

        for (const entry in data) {
            if (data.hasOwnProperty(entry)) {
                const set = data[entry];

                const dataEntry = {
                    data: set.value,
                    label: set.key,
                    backgroundColor: colors[count],
                    borderWidth: 1.2,
                    fill: false,
                };

                this.dataset.push(dataEntry);
                count++;
            }
        }

        return this.dataset;
    }

    updateDataset({ dataset }) {
        this.dataset = dataset;
    }

    getBackgroundColor() {
        return this.backgroundColor;
    }

    getTitleText() {
        return this.titleText;
    }

    print() {
        console.log('metadata::getMetadataCore::', JSON.stringify(this.getMetadataCore()));
        console.log('metadata::this.labels::', JSON.stringify(this.labels));
        console.log('metadata::this.datasetLabel::', JSON.stringify(this.datasetLabel));
        console.log('metadata::this.datasetData::', JSON.stringify(this.datasetData));
        console.log('metadata::this.backgroundColor::', JSON.stringify(this.backgroundColor));
    }
}

export class MetadataDaily implements IMetadataNEW {

    labels: string[];
    datasetLabel: string;
    datasetData: string[];
    backgroundColor = [
        '#ffffff', '#cccccc', '#ffffff', '#cccccc',
        '#ffffff', '#cccccc', '#ffffff', '#cccccc',
        '#ffffff', '#cccccc', '#ffffff', '#cccccc',
        '#ffffff', '#cccccc', '#ffffff', '#cccccc',
        '#ffffff', '#cccccc', '#ffffff', '#cccccc',
        '#ffffff', '#cccccc', '#ffffff', '#cccccc',
        '#ffffff', '#cccccc', '#ffffff', '#cccccc',
        '#ffffff', '#cccccc', '#ffffff', '#cccccc',
        '#ffffff', '#cccccc', '#ffffff', '#cccccc',
        '#ffffff', '#cccccc', '#ffffff', '#cccccc',
        '#ffffff', '#cccccc', '#ffffff', '#cccccc',
        '#ffffff', '#cccccc', '#ffffff', '#cccccc',
        '#ffffff', '#cccccc', '#ffffff', '#cccccc'];
    titleText: string;

    dataset: Array<any>;

    constructor() {
        this.titleText = 'Entrada de Pedidos';
        this.datasetLabel = 'Qnt. Pedidos';
        this.dataset = new Array<any>();
    }

    onClick(evt: any, item: any): void {
        console.log('onClick', item);
    }

    getMetadataCore() {
        return {
            type: 'line',
            data: {
                labels: this.getLabels(),
                datasets: [{
                    label: this.getDatasetLabel(),
                    data: this.getDatasetData(),
                    backgroundColor: this.getBackgroundColor(),
                    lineTension: 0,
                    pointHoverBackgroundColor: '#77f5f2',
                    pointBorderWidth: 0,
                    pointHoverRadius: 5,
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                }]
            },
            elements: {
                line: {
                    tension: 0
                }
            },
            options: {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: 'white',
                            fontSize: 8,
                            beginAtZero: false,
                            autoSkip: false
                        },
                        gridLines: {
                            display: false
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            fontColor: 'white',
                            fontSize: 8,
                            stepSize: 10,
                            beginAtZero: false,
                            autoSkip: false
                        },

                    }]
                },

                title: {
                    labels: { fontColor: '#5e908f' },
                    display: false,
                    fontColor: 'white',
                    text: this.getTitleText()
                },
                legend: {
                    labels: {
                        boxWidth: 8,
                        fontSize: 8,
                        fontColor: '#5e908f'
                    }
                },
            },
        };
    }

    getLabels() {
        return this.labels;
    }

    getDatasetLabel() {
        return this.datasetLabel;
    }

    getDatasetData() {
        return this.datasetData;
    }

    // TODO: Colors.
    assembleDataset({ aggregator, data }): any {

        this.labels = aggregator;
        this.datasetData = data;

        return this.dataset;
    }

    updateDataset({ dataset }) {
        this.dataset = dataset;
    }

    getBackgroundColor() {
        return this.backgroundColor;
    }

    getTitleText() {
        return this.titleText;
    }

    print() {
        console.log('metadata::getMetadataCore::', JSON.stringify(this.getMetadataCore()));
        console.log('metadata::this.labels::', JSON.stringify(this.labels));
        console.log('metadata::this.datasetLabel::', JSON.stringify(this.datasetLabel));
        console.log('metadata::this.datasetData::', JSON.stringify(this.datasetData));
        console.log('metadata::this.backgroundColor::', JSON.stringify(this.backgroundColor));
    }
}
