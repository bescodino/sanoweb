export class ApiSettings {

    private oldBaseUrl = 'https://webapisanofi.azurewebsites.net';
    private baseUrl = 'https://webapisanofi-dev.azurewebsites.net';
    private landingPage = '/landing-page';
    private api = '/sanofiapi.php';
    private queryType = '?queryType=';
    private subTypeChains = '&subType=rede';
    private subTypeFillRate = '&subType=FILLRATE';
    private subTypeLeadTime = '&subType=LEADTIME';

    public readonly lblCardsRuptura = 'RUPTURA';

    readonly upTop10Dist =
        this.baseUrl + this.landingPage + this.api + this.queryType + 'uptop10dist';

    readonly upTop10DistChains =
        this.baseUrl + this.landingPage + this.api + this.queryType + 'uptop10dist' + this.subTypeChains;

    readonly rupByUF =
        this.baseUrl + this.landingPage + this.api + this.queryType + 'rupbyuf';

    readonly rupByUFChains =
        this.baseUrl + this.landingPage + this.api + this.queryType + 'rupbyuf' + this.subTypeChains;

    readonly rupByNotAtEnd =
        this.baseUrl + this.landingPage + this.api + this.queryType + 'rupbynotatend';

    readonly rupByMonth =
        this.baseUrl + this.landingPage + this.api + this.queryType + 'rupbymonth';

    readonly rupByMonthChains =
        this.baseUrl + this.landingPage + this.api + this.queryType + 'rupbymonth' + this.subTypeChains;

    readonly rupByLastWeek =
        this.baseUrl + this.landingPage + this.api + this.queryType + 'rupbylastweek';

    readonly rupByLastWeekChains =
        this.baseUrl + this.landingPage + this.api + this.queryType + 'rupbylastweek' + this.subTypeChains;


    // FILLRATE & LEADTIME

    // Atendimento por BU (3 meses fechados)
    readonly fillrateByBU =
        this.baseUrl + this.landingPage + this.api + this.queryType + 'fillratebybu';

    // Entrada de Pedidos (graf 2)
    readonly fillrateByDay =
        this.baseUrl + this.landingPage + this.api + this.queryType + 'fillratebyday' + this.subTypeFillRate;

    // Leadtime por Mês
    readonly leadtimeByMonth =
        this.baseUrl + this.landingPage + this.api + this.queryType + 'leadtimebymonth';

    // LeadTime por Região
    readonly leadtimeByRegion =
        this.baseUrl + this.landingPage + this.api + this.queryType + 'leadtimebyregion' + this.subTypeLeadTime;

    // Status da Entrega
    readonly leadtimeByStatus =
        this.baseUrl + this.landingPage + this.api + this.queryType + 'leadtimebystatus' + this.subTypeLeadTime;


    card = {

        geral:
            this.baseUrl + this.landingPage + this.api + this.queryType + 'cardgeral',
        rupturaLancamentos:
            this.baseUrl + this.landingPage + this.api + this.queryType + 'cardgeral&statusvenda=lancamento',
        cestaOSA:
            this.baseUrl + this.landingPage + this.api + this.queryType + 'cardcestaosa',

        geralChains:
            this.baseUrl + this.landingPage + this.api + this.queryType + 'cardgeral' + this.subTypeChains,
        rupturaLancamentosChains:
            this.baseUrl + this.landingPage + this.api + this.queryType + 'cardgeral&statusvenda=lancamento' + this.subTypeChains,
        cestaOSAChains:
            this.baseUrl + this.landingPage + this.api + this.queryType + 'cardcestaosa' + this.subTypeChains,

        osaLastWeek:
            this.baseUrl + this.landingPage + this.api + this.queryType + 'cardlastweek',

        osaLastWeekChains:
            this.baseUrl + this.landingPage + this.api + this.queryType + 'cardlastweek' + this.subTypeChains,

    };

}
