import { ApiSettings } from './api.settings';

export class FactoryMapper {

    private settings = new ApiSettings();

    public readonly distributorsID = 'distributors';
    public readonly salesStatusID = 'sales-status';
    public readonly monthlyID = 'monthly';
    public readonly dailyID = 'daily';
    public readonly multiDatasetID = 'multi-dataset';
    public readonly leadtimeByRegion = 'leadtime-by-region';
    public readonly leadtimeByStatus = 'leadtime-by-status';


    /**
     * 1º = Id interna, 2º = URL ou identificador externo.
     */
    public readonly map: Map<string, string[]>;


    constructor() {

        this.map = new Map<string, string[]>();
        this.map.set(this.distributorsID, [this.settings.upTop10Dist, this.settings.upTop10DistChains]);
        this.map.set(this.salesStatusID, [this.settings.rupByNotAtEnd]);
        this.map.set(this.monthlyID, [this.settings.fillrateByBU]);
        this.map.set(this.dailyID, [this.settings.fillrateByDay]);
        this.map.set(this.multiDatasetID, [this.settings.leadtimeByMonth]);
        this.map.set(this.leadtimeByRegion, [this.settings.leadtimeByRegion]);
        this.map.set(this.leadtimeByStatus, [this.settings.leadtimeByStatus]);
    }

    /**
    * Dada uma URL, retorna a identificação interna.
    * @param url URL/endpoint API
    */
    getId(url: string): string {
        if (!url) { throw new Error('Exception: URL must not be null.'); }

        for (const key of Array.from(this.map.keys())) {
            if (this.map.get(key).includes(url)) {
                return key;
            }
        }

        throw new Error('Exception: URL not found.');
    }

    /**
     * Dada uma identificação interna, retorna as URL associadas.
     * @param id Identificação interna
     */
    getURLs(id: string): string[] {
        if (!id) { throw new Error('Exception: ID must not be null.'); }
        if (!this.map.has(id)) { throw new Error('Exception: ID not found.'); }

        const urls = this.map.get(id);

        if (!urls || urls == null || urls.length === 0) { throw new Error('Exception: No URLs associated to this ID.'); }

        return urls;
    }

}
