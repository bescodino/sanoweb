import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
// import { HttpClientModule, HttpClient } from '@angular/common/http';

// Main Components/Modules
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

// 3rd Party
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// i18n
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// Core Services
import { ChartsLoaderService } from './services/charts/charts-loader.service';
import { ChartsEventEmitterService } from './services/charts/charts-event-emitter.service';

// Utility Services
import { MonthsService } from './services/utility/months.service';
import { ColorsService } from './services/utility/colors.service';

// Core Components
import { AppHeaderComponent } from './app-header/app-header.component';
import { AppMenuComponent } from './app-menu/app-menu.component';
import { AppFooterComponent } from './app-footer/app-footer.component';
import { LoaderComponent } from './loader/loader.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { OsaChainsComponent } from './osa/chains/osa-chains.component';
import { WholesalerFillRateComponent } from './osa/distributors/wholesaler-fill-rate.component';

import { NewReportsComponent } from './static/new-reports/new-reports.component';
import { TestingComponent } from './testing/testing.component';

import { ChartsParserFactory } from './factories/parser/parser-factory.service';
import { ChartsFactory } from './factories/chart/chart-factory.service';
import { ContentFactoryService } from './factories/content/content-factory.service';
import { EntryFactoryService } from './factories/entry/entry-factory.service';
import { DataIdentifiersFactory } from './factories/data-identifier/data-identifiers.service';
import { ChartsMetadataFactory } from './factories/chart/chart-metadata-factory.service';
import { FillRateMainComponent } from './fill-rate/main/fill-rate-main.component';
import { ReportsComponent } from './static/reports/reports.component';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http);
}

import { TranslateService } from '@ngx-translate/core';


@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    AppMenuComponent,
    AppFooterComponent,
    LoaderComponent,
    LandingPageComponent,
    OsaChainsComponent,
    WholesalerFillRateComponent,
    ReportsComponent,
    NewReportsComponent,
    TestingComponent,
    FillRateMainComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [Http]
      }
    })
  ],
  providers: [
    // {provide: APP_BASE_HREF, useValue: ''},
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    ChartsLoaderService,
    ChartsEventEmitterService,
    MonthsService,
    ColorsService,
    ChartsParserFactory,
    ChartsFactory,
    ContentFactoryService,
    EntryFactoryService,
    ChartsMetadataFactory,
    DataIdentifiersFactory,
    TranslateService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
