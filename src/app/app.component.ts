import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Sanofi DVC';

  constructor(translate: TranslateService) {
    translate.addLangs(['pt_BR', 'en_US']);

    // this language will be used as fallback when a translation isn't found in the current language
    translate.setDefaultLang('pt_BR');

    // the lang to use, if the lang isn't available it will use the current loader to get them
    translate.use('pt_BR');

    console.log('currentLang', JSON.stringify(translate.currentLang));

  }
}
