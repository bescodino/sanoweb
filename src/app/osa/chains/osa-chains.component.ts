import { Component, ViewChild, OnInit, AfterViewInit, OnDestroy, Renderer2, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import * as Chart from 'chart.js';

import { ApiSettings } from '../../settings/api.settings';
import { LoaderComponent } from '../../loader/loader.component';

import { IChart, IChartEntry } from '../../charts/chart-entities.model';
import {
    ChartDistributor, ChartSalesStatus, ChartUF, ChartBU,
    ChartEntry, ChartEntity, ChartLineEntry, ChartLabelTexts,
    ChartLandingPage, ChartHtmlInfo
} from '../../charts/charts-implementation';
import { ChartsLoaderService } from '../../services/charts/charts-loader.service';
import { ChartsEventEmitterService } from '../../services/charts/charts-event-emitter.service';

import { MonthsService } from '../../services/utility/months.service';
import { ColorsService } from '../../services/utility/colors.service';
import { FilterQuery } from '../../filter/filter-query.component';


@Component({
    selector: 'app-osa-chains',
    templateUrl: './osa-chains.component.html',
    styleUrls: ['./osa-chains.component.css']
})
export class OsaChainsComponent implements OnInit, AfterViewInit, OnDestroy {

    settings = new ApiSettings();

    @ViewChild('chainsCardBasketLoader')
    private chainsCardBasketLoader: LoaderComponent;

    @ViewChild('chainsCardLauncherLoader')
    private chainsCardLauncherLoader: LoaderComponent;

    @ViewChild('chainsCardTotalLoader')
    private chainsCardTotalLoader: LoaderComponent;

    @ViewChild('chainsMainCustomersLoader')
    private chainsMainCustomersLoader: LoaderComponent;

    @ViewChild('chainsByStateLoader')
    private chainsByStateLoader: LoaderComponent;

    @ViewChild('chainsByBULoader')
    private chainsByBULoader: LoaderComponent;

    public salesStatusOSA: IChart;
    public distributorsOSA: IChart;
    public ufOSA: IChart;
    public buOSA: IChart;
    public buChainsSimplifiedOSA: IChart;

    public geralOSA;
    public rupturaLancamentosOSA;
    public cestaOSA;

    private filterQuery: FilterQuery;


    constructor(
        private chartsLoader: ChartsLoaderService<IChart>,
        private eventEmitter: ChartsEventEmitterService,
        private monthsService: MonthsService,
        private colorsService: ColorsService,
        private _renderer2: Renderer2,
        @Inject(DOCUMENT) private _document) {

        this.filterQuery = new FilterQuery();

        eventEmitter.graphClicked$
            .subscribe((item) => this.chartClicked(item));
    }

    ngOnInit() {
        this.showLoaders();
    }

    ngAfterViewInit() {

        this.loadCards();
        this.loadCharts();

    }

    ngOnDestroy() {
        // console.log('OSA::ONDESTROY');
        this.clear();
    }

    showLoaders() {
        this.chainsMainCustomersLoader.show();
        this.chainsByStateLoader.show();
        this.chainsByBULoader.show();

        this.chainsCardBasketLoader.show();
        this.chainsCardLauncherLoader.show();
        this.chainsCardTotalLoader.show();
    }

    chartClicked(item: any) {
        this.filterQuery.toggle(item.graphLabel, item.item[0]._model.label);
        console.log('chartClicked::query::', JSON.stringify(this.filterQuery.getQuery()));

        this.clear();
        this.showLoaders();
        this.loadCharts();
        this.loadCards();
    }


    loadCharts() {
        // Carrega gráfico para Distribuidores
        this.chartsLoader.get(this.settings.upTop10DistChains, this.filterQuery.getQuery())
            .then(response => {

                const chartHtmlInfo = new ChartHtmlInfo('distribuidorOsa');
                const chartLabels = new ChartLabelTexts(
                    'Main Customers - Last 30 days', 'Percentage', '', '');

                const chart = new ChartDistributor(
                    chartHtmlInfo, chartLabels, this.eventEmitter, 'distribuidor');

                response
                    .sort((a, b) => this.rupturaSort(a, b))
                    .forEach(element => {
                        const entry = chart.makeEntry(element[chart.lblKey], element[chart.lblValue]);
                        chart.populateContent(entry);
                    });

                this.distributorsOSA = chart;
                this.distributorsOSA.createChart();
                this.chainsMainCustomersLoader.hide();
                this.loadSliderDistributors();
            })
            .catch(response => {
                this.chainsMainCustomersLoader.hide();
                console.log('upTop10DistChains::error', JSON.stringify(response));
            });


        // Carrega gráfico para UF
        this.chartsLoader.get(this.settings.rupByUFChains, this.filterQuery.getQuery())
            .then(response => {

                const chartHtmlInfo = new ChartHtmlInfo('ufOsa');
                const chartLabels = new ChartLabelTexts(
                    'By State - Last Month', '% Rupture', '', '');

                const chart = new ChartUF(chartHtmlInfo, chartLabels, this.eventEmitter, 'uf');

                response
                    .sort((a, b) => this.rupturaSort(a, b))
                    .forEach(element => {

                        let entry;

                        if (!element[chart.lblKey] || (!element[chart.lblValue] && element[chart.lblValue] !== 0)) {
                            console.error('Empty data field found::' + JSON.stringify(element));
                            entry = chart.makeEntry('<NULL>', element[chart.lblValue]);
                        } else {
                            entry = chart.makeEntry(element[chart.lblKey], element[chart.lblValue]);
                        }

                        chart.populateContent(entry);


                        // if (element[chart.lblKey] && element[chart.lblKey] != null) {
                        //     const entry = chart.makeEntry(
                        //         element[chart.lblKey],
                        //         element[chart.lblValue]);
                        //     chart.populateContent(entry);
                        // } else {
                        //     console.error('Empty data field found::' + JSON.stringify(element));
                        // }
                    });
                this.chainsByStateLoader.hide();
                this.ufOSA = chart;
                this.ufOSA.createChart();
            })
            .catch(response => {
                this.chainsByStateLoader.hide();
                console.log('rupByUFChains::error', JSON.stringify(response));
            });


        // Carrega gráfico para BU
        this.chartsLoader.get(this.settings.rupByMonthChains, this.filterQuery.getQuery())
            .then(response => {

                const chartHtmlInfo = new ChartHtmlInfo('buOsa');
                const chartLabels = new ChartLabelTexts(
                    'By BU - Latest Months', '', '', '');

                const chart = new ChartBU(
                    chartHtmlInfo, chartLabels, this.eventEmitter, this.colorsService, 'bu');

                const months = new Array<string>();
                const labs: { [lab: string]: Array<string>; } = {};
                const dataSet = [];

                // grab month
                for (const month in response) {
                    if (response.hasOwnProperty(month)) {

                        months.push(month);

                        response[month].forEach(entry => {

                            const lab = entry[chart.lblKey];
                            const value = (entry[chart.lblValue] * 100).toFixed(2);

                            if (!labs[lab]) {
                                labs[lab] = [value];
                            } else {
                                // Funny workaround code
                                const hackyMchackerson = labs[lab];
                                hackyMchackerson.push(value as string);
                                labs[lab] = hackyMchackerson;
                            }
                        });
                    }
                }

                const entries = new Array<IChartEntry>();
                for (const lab in labs) {
                    if (labs.hasOwnProperty(lab)) {
                        const data = labs[lab];
                        entries.push(chart.makeEntry(lab, data));
                    }
                }

                chart.populateContent({ lines: months, data: entries });

                // chart.print();
                this.buOSA = chart;
                this.buOSA.createChart();
                this.chainsByBULoader.hide();

            })
            .catch(response => {
                this.chainsByBULoader.hide();
                console.log('rupByMonthChains::error', JSON.stringify(response));
            });

        // Carrega gráfico para BU Simplificado
        this.chartsLoader.get(this.settings.rupByLastWeekChains, this.filterQuery.getQuery())
            .then(response => {

                const chartHtmlInfo = new ChartHtmlInfo('buSimplifiedOsa');
                const chartLabels = new ChartLabelTexts(
                    'By BU - Latest Months', 'Percentage', '', 'Last week');

                const chart = new ChartLandingPage(
                    chartHtmlInfo, 'bu-simplified-osa', 'BU', 'RUPTURA', chartLabels, this.eventEmitter);

                response
                    // .sort((a, b) => this.rupturaSort(a, b))
                    .forEach(element => {
                        const entry = chart.makeEntry(element[chart.lblKey], element[chart.lblValue]);
                        chart.populateContent(entry);
                    });

                // this.buSimplifiedLoader.hide();

                this.buChainsSimplifiedOSA = chart;
                this.loadSliderBUSimplified();
                // this.buSimplified.createChart();
            })
            .catch(response => {
                // this.buSimplifiedLoader.hide();
                console.log('WholesalerFillRate::error', JSON.stringify(response));
            });
    }

    loadCards() {
        // Card Geral
        this.chartsLoader.get(this.settings.card.geralChains, this.filterQuery.getQuery())
            .then(response => {
                this.chainsCardTotalLoader.hide();
                const r = response[0][this.settings.lblCardsRuptura];
                this.geralOSA = (r * 100).toFixed(1) + '%';
            })
            .catch(response => {
                this.chainsCardTotalLoader.hide();
                console.log('error', JSON.stringify(response));
            });

        // Card Lançamentos
        this.chartsLoader.get(this.settings.card.rupturaLancamentosChains, this.filterQuery.getQuery())
            .then(response => {
                this.chainsCardLauncherLoader.hide();
                const r = response[0][this.settings.lblCardsRuptura];
                this.rupturaLancamentosOSA = (r * 100).toFixed(1) + '%';
            })
            .catch(response => {
                this.chainsCardLauncherLoader.hide();
                console.log('error', JSON.stringify(response));
            });

        // Card Cesta
        this.chartsLoader.get(this.settings.card.cestaOSAChains, this.filterQuery.getQuery())
            .then(response => {
                this.chainsCardBasketLoader.hide();
                const r = response[0][this.settings.lblCardsRuptura];
                this.cestaOSA = (r * 100).toFixed(1) + '%';
            })
            .catch(response => {
                this.chainsCardBasketLoader.hide();
                console.log('error', JSON.stringify(response));
            });

    }

    loadSliderBUSimplified() {
        // if (!document.getElementById('slickBoard02')) { return; }
        // console.log('slickBoard02::childnodes', JSON.stringify(document.getElementById('slickBoard02').childNodes));

        // if (!document.getElementById('slickBoard02').hasChildNodes()) {
        //     console.log('slickBoard02::childnodes', JSON.stringify(document.getElementById('slickBoard02').childNodes));
        // }

        const script = this._renderer2.createElement('script');
        script.type = `text/javascript`;
        script.text = `
            {
                $(document).ready(function() {

                    var slider = $(".slickBoard02").lightSlider({
                        item:4,
                        loop:false,
                        pager:false,
                        slideMove:2,
                        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
                        speed:600,
                        responsive : [
                            {
                                breakpoint:800,
                                settings: {
                                    item:3,
                                    slideMove:1,
                                    slideMargin:6,
                                  }
                            },
                            {
                                breakpoint:480,
                                settings: {
                                    item:4,
                                    slideMove:4
                                  }
                            }
                        ]
                    }); // declare var again

                });
            }
        `;

        this._renderer2.appendChild(this._document.body, script);
    }

    loadSliderDistributors() {
        // if (!document.getElementById('slickBoard02')) { return; }
        // console.log('slickBoard03::childnodes', JSON.stringify(document.getElementById('slickBoard03').childNodes));

        const script = this._renderer2.createElement('script');
        script.type = `text/javascript`;
        script.text = `
           {
                $(document).ready(function() {

                    var slider = $(".slickBoard03").lightSlider({
                        item:4,
                        loop:false,
                        pager:false,
                        slideMove:2,
                        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
                        speed:600,
                        responsive : [
                            {
                                breakpoint:800,
                                settings: {
                                    item:3,
                                    slideMove:1,
                                    slideMargin:6,
                                }
                            },
                            {
                                breakpoint:480,
                                settings: {
                                    item:2,
                                    slideMove:2
                                }
                            }
                        ]
                    }); // declare var again
                });
           }
       `;

        this._renderer2.appendChild(this._document.body, script);
    }

    getClass(index: number): string {
        return 'bgColor' + index.toString();
    }

    clear() {
        if (this.salesStatusOSA) {
            try {
                this.salesStatusOSA.destroyChart();
            } catch (error) {
                console.log('destroyChart::Chart not found?');
            }
        };

        if (this.distributorsOSA) {
            try {
                this.distributorsOSA.destroyChart();
            } catch (error) {
                console.log('destroyChart::Chart not found?');
            }
        };

        if (this.ufOSA) {
            try {
                this.ufOSA.destroyChart();
            } catch (error) {
                console.log('destroyChart::Chart not found?');
            }
        };

        if (this.buOSA) {
            try {
                this.buOSA.destroyChart();
            } catch (error) {
                console.log('destroyChart::Chart not found?');
            }
        };

        // if (this.salesStatusOSA) { this.salesStatusOSA.destroyChart(); };
        // if (this.distributorsOSA) { this.distributorsOSA.destroyChart(); };
        // if (this.ufOSA) { this.ufOSA.destroyChart(); };
        // if (this.buOSA) { this.buOSA.destroyChart(); };

        this.geralOSA = null;
        this.rupturaLancamentosOSA = null;
        this.cestaOSA = null;
    }

    correctValue(value: string) {
        let _value = value;

        if (_value) {
            if (_value.endsWith('00')) {
                _value = _value.substring(0, _value.length - 3);
            }
        }

        return _value;
    }

    rupturaSort(elementA, elementB) {
        if (elementA.RUPTURA > elementB.RUPTURA) {
            return -1;
        }

        if (elementA.RUPTURA < elementB.RUPTURA) {
            return 1;
        }

        return 0;
    }

}
