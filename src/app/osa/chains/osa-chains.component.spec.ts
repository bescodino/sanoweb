import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OsaChainsComponent } from './osa-chains.component';

describe('OsaChainsComponent', () => {
  let component: OsaChainsComponent;
  let fixture: ComponentFixture<OsaChainsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OsaChainsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OsaChainsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
