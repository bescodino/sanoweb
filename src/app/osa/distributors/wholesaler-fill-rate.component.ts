import { Component, ViewChild, OnInit, AfterViewInit, OnDestroy, Renderer2, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import * as Chart from 'chart.js';

import { ApiSettings } from '../../settings/api.settings';
import { LoaderComponent } from '../../loader/loader.component';

import { ChartsLoaderService } from '../../services/charts/charts-loader.service';
import { ChartsEventEmitterService } from '../../services/charts/charts-event-emitter.service';
import { IChart, IChartEntry } from '../../charts/chart-entities.model';
import {
    ChartLabelTexts, ChartSalesStatus,
    ChartDistributor, ChartUF, ChartBU,
    ChartLandingPage, ChartHtmlInfo
} from '../../charts/charts-implementation';

import { MonthsService } from '../../services/utility/months.service';
import { ColorsService } from '../../services/utility/colors.service';
import { FilterQuery } from '../../filter/filter-query.component';

@Component({
    selector: 'app-wholesaler-fill-rate',
    templateUrl: './wholesaler-fill-rate.component.html',
    styleUrls: ['./wholesaler-fill-rate.component.css']
})

export class WholesalerFillRateComponent implements OnInit, AfterViewInit, OnDestroy {

    settings = new ApiSettings();

    @ViewChild('wholesalerCardBasketLoader')
    private wholesalerCardBasketLoader: LoaderComponent;

    @ViewChild('wholesalerCardLauncherLoader')
    private wholesalerCardLauncherLoader: LoaderComponent;

    @ViewChild('wholesalerCardTotalLoader')
    private wholesalerCardTotalLoader: LoaderComponent;

    @ViewChild('wholesalerDashMainCostumersLoader')
    private wholesalerDashMainCostumersLoader: LoaderComponent;

    @ViewChild('wholesalerDashByStateLoader')
    private wholesalerDashByStateLoader: LoaderComponent;

    @ViewChild('wholesalerDashByBULoader')
    private wholesalerDashByBULoader: LoaderComponent;

    @ViewChild('wholesalerDashReasonsLoader')
    private wholesalerDashReasonsLoader: LoaderComponent;

    public salesStatus: IChart;
    public distributors: IChart;
    public uf: IChart;
    public bu: IChart;
    public buSimplified: IChart;

    public geral: string;
    public rupturaLancamentos: string;
    public cesta: string;

    private filterQuery: FilterQuery;


    constructor(
        private chartsLoader: ChartsLoaderService<IChart>,
        private eventEmitter: ChartsEventEmitterService,
        private monthsService: MonthsService,
        private colorsService: ColorsService,
        private _renderer2: Renderer2,
        @Inject(DOCUMENT) private _document) {

        this.filterQuery = new FilterQuery();

        eventEmitter.graphClicked$
            .subscribe((item) => this.chartClicked(item));
    }


    ngOnInit() {
        this.showLoaders();
    }

    ngAfterViewInit() {
        this.loadCards();
        this.loadCharts();
    }

    ngOnDestroy() {
        console.log('FR::ONDESTROY');
        this.clear();
    }

    chartClicked(item: any) {
        this.filterQuery.toggle(item.graphLabel, item.item[0]._model.label);
        // console.log('chartClicked::query::', JSON.stringify(this.filterQuery.getQuery()));

        this.clear();
        this.showLoaders();
        this.loadCharts();
        this.loadCards();
    }


    showLoaders() {
        this.wholesalerDashMainCostumersLoader.show();
        this.wholesalerDashByStateLoader.show();
        this.wholesalerDashByBULoader.show();
        this.wholesalerDashReasonsLoader.show();

        this.wholesalerCardBasketLoader.show();
        this.wholesalerCardLauncherLoader.show();
        this.wholesalerCardTotalLoader.show();
    }


    loadCharts() {

        // this.wholesalerDashMainCostumersLoader.show();
        // this.wholesalerDashByStateLoader.show();
        // this.wholesalerDashByBULoader.show();
        // this.wholesalerDashReasonsLoader.show();

        // Carrega gráfico para Status Venda
        this.chartsLoader.get(this.settings.rupByNotAtEnd, this.filterQuery.getQuery())
            .then(response => {
                const chartHtmlInfo = new ChartHtmlInfo('statusVendaFr');
                const chartLabels = new ChartLabelTexts(
                    'Reasons - Last Month', 'Percentage', '', 'Last week');

                const chart = new ChartSalesStatus(
                    chartHtmlInfo, chartLabels, this.eventEmitter, 'statusvenda');

                response
                    // .sort((a, b) => this.rupturaSort(a, b))
                    .forEach(element => {
                        let entry;
                        console.log('element:::', JSON.stringify(element));
                        if (!element[chart.lblKey] || (!element[chart.lblValue] && element[chart.lblValue] !== 0)) {
                            console.error('Empty data field found::' + JSON.stringify(element));
                            entry = chart.makeEntry(this.sanitizeKey('<NULL>'), element[chart.lblValue]);
                        } else {
                            entry = chart.makeEntry(this.sanitizeKey(element[chart.lblKey]), element[chart.lblValue]);
                        }

                        chart.populateContent(entry);

                    });

                this.salesStatus = chart;
                this.salesStatus.createChart();
                this.wholesalerDashReasonsLoader.hide();
            })
            .catch(response => {
                this.wholesalerDashReasonsLoader.hide();
                console.log('WholesalerFillRate::error', JSON.stringify(response));
            });


        // Carrega gráfico para Distribuidores
        this.chartsLoader.get(this.settings.upTop10Dist, this.filterQuery.getQuery())
            .then(response => {
                const chartHtmlInfo = new ChartHtmlInfo('distribuidorFr');
                const chartLabels = new ChartLabelTexts(
                    'Main Customers - Last Month', 'Percentage', '', 'Last week');

                const chart = new ChartDistributor(
                    chartHtmlInfo, chartLabels, this.eventEmitter, 'distribuidor');

                response
                    // .sort((a, b) => this.rupturaSort(a, b))
                    .forEach(element => {
                        let entry;

                        if (!element[chart.lblKey] || (!element[chart.lblValue] && element[chart.lblValue] !== 0)) {
                            console.error('Empty data field found::' + JSON.stringify(element));
                            entry = chart.makeEntry('<NULL>', element[chart.lblValue]);
                        } else {
                            entry = chart.makeEntry(element[chart.lblKey], element[chart.lblValue]);
                        }

                        chart.populateContent(entry);
                    });

                this.wholesalerDashMainCostumersLoader.hide();
                this.distributors = chart;
                this.distributors.createChart();

                this.loadSliderDistributors();
            })
            .catch(response => {
                this.wholesalerDashMainCostumersLoader.hide();
                console.log('WholesalerFillRate::error', JSON.stringify(response));
            });


        // Carrega gráfico para UF
        this.chartsLoader.get(this.settings.rupByUF, this.filterQuery.getQuery())
            .then(response => {
                const chartHtmlInfo = new ChartHtmlInfo('ufFr');
                const chartLabels = new ChartLabelTexts(
                    'By BU - Latest Months', 'Percentage', '', 'Last week');

                const chart = new ChartUF(chartHtmlInfo, chartLabels, this.eventEmitter, 'uf');

                response
                    // .sort((a, b) => this.rupturaSort(a, b))
                    .forEach(element => {
                        let entry;

                        if (!element[chart.lblKey] || (!element[chart.lblValue] && element[chart.lblValue] !== 0)) {
                            console.error('Empty data field found::' + JSON.stringify(element));
                            entry = chart.makeEntry('<NULL>', element[chart.lblValue]);
                        } else {
                            entry = chart.makeEntry(element[chart.lblKey], element[chart.lblValue]);
                        }

                        chart.populateContent(entry);
                    });
                this.wholesalerDashByStateLoader.hide();
                this.uf = chart;
                this.uf.createChart();
            })
            .catch(response => {
                this.wholesalerDashByStateLoader.hide();
                console.log('WholesalerFillRate::error', JSON.stringify(response));
            });


        // Carrega gráfico para BU
        this.chartsLoader.get(this.settings.rupByMonth, this.filterQuery.getQuery())
            .then(response => {
                const chartHtmlInfo = new ChartHtmlInfo('buFr');
                const chartLabels = new ChartLabelTexts(
                    'By BU - Latest Months', 'Percentage', '', 'Last week');

                const chart = new ChartBU(
                    chartHtmlInfo, chartLabels, this.eventEmitter, this.colorsService);

                const months = new Array<string>();
                const labs: { [lab: string]: Array<string>; } = {};
                const dataSet = [];

                for (const month in response) {
                    if (response.hasOwnProperty(month)) {

                        months.push(month);

                        response[month].forEach(entry => {

                            const lab = entry[chart.lblKey];
                            const value = (entry[chart.lblValue] * 100).toFixed(2);

                            if (!labs[lab]) {
                                labs[lab] = [value];
                            } else {
                                // Funny workaround code
                                const hackyMchackerson = labs[lab];
                                hackyMchackerson.push(value as string);
                                labs[lab] = hackyMchackerson;
                            }
                        });
                    }
                }

                const entries = new Array<IChartEntry>();
                for (const lab in labs) {
                    if (labs.hasOwnProperty(lab)) {
                        const data = labs[lab];
                        entries.push(chart.makeEntry(lab, data));
                    }
                }

                chart.populateContent({ lines: months, data: entries });

                this.bu = chart;
                this.bu.createChart();
                this.wholesalerDashByBULoader.hide();

            })
            .catch(response => {
                this.wholesalerDashByBULoader.hide();
                console.log('WholesalerFillRate::error', JSON.stringify(response));
            });


        // Carrega gráfico para BU Simplificado
        this.chartsLoader.get(this.settings.rupByLastWeek, this.filterQuery.getQuery())
            .then(response => {
                const chartHtmlInfo = new ChartHtmlInfo('buSimplifiedFr');
                const chartLabels = new ChartLabelTexts(
                    'By BU - Latest Months', 'Percentage', '', 'Last week');

                const chart = new ChartLandingPage(
                    chartHtmlInfo, 'bu-simplified', 'BU', 'RUPTURA', chartLabels, this.eventEmitter);

                response
                    // .sort((a, b) => this.rupturaSort(a, b))
                    .forEach(element => {
                        const entry = chart.makeEntry(element[chart.lblKey], element[chart.lblValue]);
                        chart.populateContent(entry);
                    });

                // this.buSimplifiedLoader.hide();

                this.buSimplified = chart;
                this.loadSliderBUSimplified();
                // this.buSimplified.createChart();
            })
            .catch(response => {
                // this.buSimplifiedLoader.hide();
                console.log('WholesalerFillRate::error', JSON.stringify(response));
            });

    }


    loadCards() {

        // this.wholesalerCardBasketLoader.show();
        // this.wholesalerCardLauncherLoader.show();
        // this.wholesalerCardTotalLoader.show();

        // Carregar cards
        this.chartsLoader.get(this.settings.card.geral, this.filterQuery.getQuery())
            .then(response => {

                this.wholesalerCardTotalLoader.hide();
                const r = response[0][this.settings.lblCardsRuptura];
                this.geral = this.correctValue((r * 100).toFixed(1)) + '%';
            })

            .catch(response => {
                this.wholesalerCardTotalLoader.hide();
                console.log('WholesalerFillRate::error', JSON.stringify(response));
            });

        this.chartsLoader.get(this.settings.card.rupturaLancamentos, this.filterQuery.getQuery())
            .then(response => {
                this.wholesalerCardLauncherLoader.hide();
                const r = response[0][this.settings.lblCardsRuptura];
                this.rupturaLancamentos = this.correctValue((r * 100).toFixed(1)) + '%';
            })
            .catch(response => {
                this.wholesalerCardLauncherLoader.hide();
                console.log('WholesalerFillRate::error', JSON.stringify(response));
            });

        this.chartsLoader.get(this.settings.card.cestaOSA, this.filterQuery.getQuery())
            .then(response => {
                this.wholesalerCardBasketLoader.hide();
                const r = response[0][this.settings.lblCardsRuptura];
                this.cesta = this.correctValue((r * 100).toFixed(1)) + '%';
            })
            .catch(response => {
                this.wholesalerCardBasketLoader.hide();
                console.log('WholesalerFillRate::error', JSON.stringify(response));
            });
    }


    loadSliderDistributors() {
        // if (!document.getElementById('slickBoard01')) { return; }

        const script = this._renderer2.createElement('script');

        script.type = `text/javascript`;
        script.text = `
            {
                $(document).ready(function() {

                    var slider = $(".slickBoard01").lightSlider({
                        item:4,
                        loop:false,
                        pager:false,
                        slideMove:2,
                        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
                        speed:600,
                        responsive : [
                            {
                                breakpoint:800,
                                settings: {
                                    item:3,
                                    slideMove:1,
                                    slideMargin:6,
                                }
                            },
                            {
                                breakpoint:480,
                                settings: {
                                    item:3,
                                    slideMove:3
                                }
                            }
                        ]
                    }); // declare var again

                });
            }
        this._renderer2.appendChild(this._document.body, script);
        `;

        try {
            this._renderer2.appendChild(this._document.body, script);
            console.log('doc', this._document.body);
            console.log('script', script);
        } catch (error) {
            console.log('err::', JSON.stringify(this._document.body, script));
        }
    }

    loadSliderBUSimplified() {
        // if (!document.getElementById('slickBoard02')) { return; }

        const script = this._renderer2.createElement('script');
        script.type = `text/javascript`;
        script.text = `
            {
                $(document).ready(function() {

                    var slider = $(".slickBoard02").lightSlider({
                        item:4,
                        loop:false,
                        pager:false,
                        slideMove:2,
                        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
                        speed:600,
                        responsive : [
                            {
                                breakpoint:800,
                                settings: {
                                    item:3,
                                    slideMove:1,
                                    slideMargin:6,
                                  }
                            },
                            {
                                breakpoint:480,
                                settings: {
                                    item:4,
                                    slideMove:4
                                  }
                            }
                        ]
                    }); // declare var again

                });
            }
        `;

        this._renderer2.appendChild(this._document.body, script);
    }


    getClass(index: number): string {
        return 'bgColor' + index.toString();
    }

    clear() {

        if (this.salesStatus) {

            try {
                this.salesStatus.destroyChart();
            } catch (error) {
                console.log('destroyChart::Chart not found?');
            }

        };

        if (this.distributors) {
            try {
                this.distributors.destroyChart();
            } catch (error) {
                console.log('destroyChart::Chart not found?');
            }
        };

        if (this.uf) {
            try {
                this.uf.destroyChart();
            } catch (error) {
                console.log('destroyChart::Chart not found?');
            }
        };

        if (this.bu) {
            try {
                this.bu.destroyChart();
            } catch (error) {
                console.log('destroyChart::Chart not found?');
            }
        };

        this.geral = null;
        this.rupturaLancamentos = null;
        this.cesta = null;
    }


    sanitizeKey(item: string): string {

        if (!item) { console.error('Exception: Null Label encountered'); }

        const label = item.replace(/\s+/g, '');

        // console.log('label::b4::', JSON.stringify(item));
        // console.log('label::aft::', JSON.stringify(label));

        /* Tratamento das Labels */
        if (label === 'FALTADEESTOQUE') {
            return 'OOS';
        }
        if (label === 'PEDIDOMINIMO') {
            return 'Minimum Order';
        }
        if (label === 'DOCUMENTACAO') {
            return 'Documentation';
        }
        if (label === 'MOTIVONAOINFORMADO') {
            return 'Others';
        }
        if (label === 'PEDIDODUPLICADO') {
            return 'Duplicated Order';
        }
        if (label === 'LAYOUTINCORRETO') {
            return 'Wrong Layout';
        }
        if (label === 'QUANTIDADESOLICITADAINVALIDA') {
            return 'Invalid Quantity';
        }
        if (label === 'PRODUTODESCONTINUADO') {
            return 'Discontinued Product';
        }
        if (label === 'CADASTRO(PRODUTO/CLIENTE)/LIMITEDECREDITO') {
            return 'Credit Limit';
        }
        if (label === 'VALIDADECURTA') {
            return 'Short Shelf Life';
        }


        console.error('Exception: Unknown Label encountered', JSON.stringify(item));
        return item;

    }

    correctValue(value: string) {
        let _value = value;

        if (_value) {
            if (_value.endsWith('00')) {
                _value = _value.substring(0, _value.length - 3);
            }
        }

        return _value;
    }

    rupturaSort(elementA, elementB) {
        if (elementA.RUPTURA > elementB.RUPTURA) {
            return -1;
        }

        if (elementA.RUPTURA < elementB.RUPTURA) {
            return 1;
        }

        return 0;
    }

}
