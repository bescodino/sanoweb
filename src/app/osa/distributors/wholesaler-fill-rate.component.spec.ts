import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WholesalerFillRateComponent } from './wholesaler-fill-rate.component';

describe('WholesalerFillRateComponent', () => {
  let component: WholesalerFillRateComponent;
  let fixture: ComponentFixture<WholesalerFillRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WholesalerFillRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WholesalerFillRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
