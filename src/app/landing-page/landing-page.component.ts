import { Component, ViewChild, AfterViewInit, OnInit } from '@angular/core';
import * as Chart from 'chart.js';
import { Router, ActivatedRoute } from '@angular/router';

import { ApiSettings } from '../settings/api.settings';
import { LoaderComponent } from '../loader/loader.component';

import { IChart, IChartEntry } from '../charts/chart-entities.model';
import { ChartLandingPage, ChartLabelTexts, ChartHtmlInfo } from '../charts/charts-implementation';
import { ChartsLoaderService } from '../services/charts/charts-loader.service';
import { ChartsEventEmitterService } from '../services/charts/charts-event-emitter.service';

import { MonthsService } from '../services/utility/months.service';
import { ColorsService } from '../services/utility/colors.service';


@Component({
    selector: 'app-landing-page',
    templateUrl: './landing-page.component.html',
    styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit, AfterViewInit {

    settings = new ApiSettings();

    @ViewChild('osaChainsCardsLoader')
    private osaChainsCardsLoader: LoaderComponent;

    @ViewChild('fillRateCardsLoader')
    private fillRateCardsLoader: LoaderComponent;

    @ViewChild('osaChainsLoader')
    private osaChainsLoader: LoaderComponent;

    @ViewChild('fillRateLoader')
    private fillRateLoader: LoaderComponent;

    osaChains: IChart;
    public osaTotal: string;

    fillRate: IChart;
    public fillRateTotal: string;

    readonly osaChainsLabelURL = 'chains/osa';
    readonly fillRateLabelURL = 'fill-rate/wholesaler';


    constructor(
        private chartsLoader: ChartsLoaderService<IChart>,
        private eventEmitter: ChartsEventEmitterService,
        private monthsService: MonthsService,
        private colorsService: ColorsService,
        private router: Router
    ) {
        eventEmitter.graphClicked$
            .subscribe((item) => this.graphClicked(item));
    }

    graphClicked(item: any) {
        if (!item.graphLabel) { return; };
        this.router.navigate([item.graphLabel]);
    }

    rupturaSort(elementA, elementB) {
        if (elementA.RUPTURA > elementB.RUPTURA) {
            return -1;
        }

        if (elementA.RUPTURA < elementB.RUPTURA) {
            return 1;
        }

        return 0;
    }

    ngOnInit() {
        this.loadCards();
        this.loadCharts();
    }

    loadCharts() {
        this.osaChainsLoader.show();
        this.fillRateLoader.show();

        const chartLabels = new ChartLabelTexts(
            '', '% Rupture Wholesaler', '', 'Last week');

        this.chartsLoader.get(this.settings.rupByLastWeekChains)
            .then(response => {
                const chartHtmlInfo = new ChartHtmlInfo('osaChains');

                // OSA by BU
                const chart = new ChartLandingPage(
                    chartHtmlInfo, this.osaChainsLabelURL, 'BU', 'RUPTURA', chartLabels, this.eventEmitter);

                response
                    .sort((a, b) => this.rupturaSort(a, b))
                    .forEach(element => {
                        const entry = chart.makeEntry(element[chart.lblKey], element[chart.lblValue]);
                        chart.populateContent(entry);
                    });

                this.osaChainsLoader.hide();

                this.osaChains = chart;
                this.osaChains.createChart();
            })
            .catch(response => {
                this.osaChainsLoader.hide();
                console.log('landing-page::error', JSON.stringify(response));
            });

        this.chartsLoader.get(this.settings.rupByLastWeek)
            .then(response => {

                const chartHtmlInfo = new ChartHtmlInfo('fillRate');
                // Fill rate by BU
                const chart = new ChartLandingPage(
                    chartHtmlInfo, this.fillRateLabelURL, 'BU', 'RUPTURA', chartLabels, this.eventEmitter);

                response
                    .sort((a, b) => this.rupturaSort(a, b))
                    .forEach(element => {
                        const entry = chart.makeEntry(element[chart.lblKey], element[chart.lblValue]);
                        chart.populateContent(entry);
                    });

                this.fillRateLoader.hide();

                this.fillRate = chart;
                this.fillRate.createChart();
            })
            .catch(response => {
                this.fillRateLoader.hide();
                console.log('landing-page::error', JSON.stringify(response));
            });
    }

    loadCards() {
        this.osaChainsCardsLoader.show();
        this.fillRateCardsLoader.show();

        this.chartsLoader.get(this.settings.card.osaLastWeek)
            .then(response => {
                this.fillRateCardsLoader.hide();
                const r = response[0][this.settings.lblCardsRuptura];
                this.fillRateTotal = (r * 100).toFixed(1) + '%';
            })
            .catch(response => {
                this.fillRateCardsLoader.hide();
                console.log('landing-page::error', JSON.stringify(response));
            });

        this.chartsLoader.get(this.settings.card.osaLastWeekChains)
            .then(response => {
                this.osaChainsCardsLoader.hide();
                const r = response[0][this.settings.lblCardsRuptura];
                this.osaTotal = (r * 100).toFixed(1) + '%';
            })
            .catch(response => {
                this.osaChainsCardsLoader.hide();
                console.log('landing-page::error', JSON.stringify(response));
            });
    }

    ngAfterViewInit() { }

}
