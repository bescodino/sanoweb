import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'app-new-reports',
  templateUrl: './new-reports.component.html',
  styleUrls: ['./new-reports.component.css']
})
export class NewReportsComponent implements OnInit, OnDestroy {

  sub: any;
  report: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router) { }

    ngOnInit() {
      this.sub = this.route
        .queryParams
        .subscribe(params => {
          this.report = +params['report'] || 0;
        });
    }

    ngOnDestroy() {
      this.sub.unsubscribe();
    }

    nextPage() {
      // this.router.navigate(['product-list'], { queryParams: { page: this.page + 1 } });
    }

}
