import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { OsaChainsComponent } from './osa/chains/osa-chains.component';
import { WholesalerFillRateComponent } from './osa/distributors/wholesaler-fill-rate.component';

import { LandingPageComponent } from 'app/landing-page/landing-page.component';
import { ReportsComponent } from './static/reports/reports.component';
import { NewReportsComponent } from './static/new-reports/new-reports.component';
import { TestingComponent } from './testing/testing.component';

import { FillRateMainComponent } from './fill-rate/main/fill-rate-main.component';

const routes: Routes = [
    { path: 'landing',              component: LandingPageComponent, data: { title: 'Main Dashboard' } },
    { path: 'chains/osa',           component: OsaChainsComponent, data: { title: 'OSA Chains' } },
    { path: 'fill-rate/wholesaler', component: WholesalerFillRateComponent, data: { title: 'Wholesaler Fill Rate Tier 1' } },
    { path: 'reports/bi',           component: ReportsComponent, data: { title: 'Power BI' } },
    { path: 'testing',              component: TestingComponent, data: { title: 'AT WORK' } },
    { path: 'leadtime',              component: FillRateMainComponent, data: { title: 'Lead Time e Fill Rate'} },
    {
        path: '',
        redirectTo: '/landing',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        CommonModule,
        // RouterModule.forRoot(routes)
        RouterModule.forRoot(routes, {useHash: true})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
