export class MOCK {

    public upTop10DistA = [
        {
            'DISTRIBUIDOR': 'STA CRUZ',
            'RUPTURA': 0.642586086500829
        },
        {
            'DISTRIBUIDOR': 'SERVIMED',
            'RUPTURA': 0.557266726720083
        },
        {
            'DISTRIBUIDOR': 'PANPHARMA',
            'RUPTURA': 0.486649800463644
        },
        {
            'DISTRIBUIDOR': 'PROFARMA',
            'RUPTURA': 0.523104923500118
        },
        {
            'DISTRIBUIDOR': 'GRUPO JORGE BATISTA',
            'RUPTURA': 0.596350093923376
        },
        {
            'DISTRIBUIDOR': 'ORGAFARMA',
            'RUPTURA': 0.444620775201729
        },
        {
            'DISTRIBUIDOR': 'D CENTER',
            'RUPTURA': 0.418895677782653
        },
        {
            'DISTRIBUIDOR': 'SOLFARMA',
            'RUPTURA': 0.67600803365177
        },
        {
            'DISTRIBUIDOR': 'EMEFARMA',
            'RUPTURA': 0.529327656688582
        },
        {
            'DISTRIBUIDOR': 'DIMED',
            'RUPTURA': 0.606565672622454
        },
        {
            'DISTRIBUIDOR': 'IMIFARMA',
            'RUPTURA': 0.825952671835751
        },
        {
            'DISTRIBUIDOR': 'TAPAJOS',
            'RUPTURA': 0.787737137091735
        },
        {
            'DISTRIBUIDOR': 'ANBFARMA',
            'RUPTURA': 0.384016219221085
        },
        {
            'DISTRIBUIDOR': 'PROSPER',
            'RUPTURA': 0.442628603256137
        },
        {
            'DISTRIBUIDOR': 'TOTAL',
            'RUPTURA': 0.614175429127673
        }
    ];

    public upTop10DistB = [
        {
            'DISTRIBUIDOR': 'STA CRUZ',
            'RUPTURA': 0.418895677782653
        },
        {
            'DISTRIBUIDOR': 'SERVIMED',
            'RUPTURA': 0.557266726720083
        },
        {
            'DISTRIBUIDOR': 'PANPHARMA',
            'RUPTURA': 0.486649800463644
        },
        {
            'DISTRIBUIDOR': 'PROFARMA',
            'RUPTURA': 0.523104923500118
        },
        {
            'DISTRIBUIDOR': 'GRUPO JORGE BATISTA',
            'RUPTURA': 0.596350093923376
        },
        {
            'DISTRIBUIDOR': 'ORGAFARMA',
            'RUPTURA': 0.444620775201729
        },
        {
            'DISTRIBUIDOR': 'D CENTER',
            'RUPTURA': 0.642586086500829
        },
        {
            'DISTRIBUIDOR': 'SOLFARMA',
            'RUPTURA': 0.825952671835751
        },
        {
            'DISTRIBUIDOR': 'EMEFARMA',
            'RUPTURA': 0.529327656688582
        },
        {
            'DISTRIBUIDOR': 'DIMED',
            'RUPTURA': 0.606565672622454
        },
        {
            'DISTRIBUIDOR': 'IMIFARMA',
            'RUPTURA': 0.384016219221085
        },
        {
            'DISTRIBUIDOR': 'TAPAJOS',
            'RUPTURA': 0.787737137091735
        },
        {
            'DISTRIBUIDOR': 'ANBFARMA',
            'RUPTURA': 0.67600803365177
        },
        {
            'DISTRIBUIDOR': 'PROSPER',
            'RUPTURA': 0.442628603256137
        },
        {
            'DISTRIBUIDOR': 'TOTAL',
            'RUPTURA': 0.614175429127673
        }
    ];

    public upTop10DistC = [
        {
            'DISTRIBUIDOR': 'STA CRUZ',
            'RUPTURA': 0.67600803365177
        },
        {
            'DISTRIBUIDOR': 'SERVIMED',
            'RUPTURA': 0.557266726720083
        },
        {
            'DISTRIBUIDOR': 'PANPHARMA',
            'RUPTURA': 0.486649800463644
        },
        {
            'DISTRIBUIDOR': 'PROFARMA',
            'RUPTURA': 0.523104923500118
        },
        {
            'DISTRIBUIDOR': 'GRUPO JORGE BATISTA',
            'RUPTURA': 0.596350093923376
        },
        {
            'DISTRIBUIDOR': 'ORGAFARMA',
            'RUPTURA': 0.444620775201729
        },
        {
            'DISTRIBUIDOR': 'D CENTER',
            'RUPTURA': 0.642586086500829
        },
        {
            'DISTRIBUIDOR': 'SOLFARMA',
            'RUPTURA': 0.614175429127673
        },
        {
            'DISTRIBUIDOR': 'EMEFARMA',
            'RUPTURA': 0.529327656688582
        },
        {
            'DISTRIBUIDOR': 'DIMED',
            'RUPTURA': 0.606565672622454
        },
        {
            'DISTRIBUIDOR': 'IMIFARMA',
            'RUPTURA': 0.825952671835751
        },
        {
            'DISTRIBUIDOR': 'TAPAJOS',
            'RUPTURA': 0.418895677782653
        },
        {
            'DISTRIBUIDOR': 'ANBFARMA',
            'RUPTURA': 0.442628603256137
        },
        {
            'DISTRIBUIDOR': 'PROSPER',
            'RUPTURA': 0.787737137091735
        },
        {
            'DISTRIBUIDOR': 'TOTAL',
            'RUPTURA': 0.384016219221085
        }
    ];


    public rupbynotatendA = [
        {
            'STATUS VENDA': 'FALTA DE ESTOQUE',
            'RUPTURA': 0.942
        },
        {
            'STATUS VENDA': 'CADASTRO (PRODUTO\/CLIENTE) \/ LIMITE DE CREDITO',
            'RUPTURA': 0.041
        },
        {
            'STATUS VENDA': 'PEDIDO MINIMO',
            'RUPTURA': 0.008
        },
        {
            'STATUS VENDA': 'DOCUMENTACAO',
            'RUPTURA': 0.005
        },
        {
            'STATUS VENDA': 'MOTIVO NAO INFORMADO',
            'RUPTURA': 0.003
        },
        {
            'STATUS VENDA': 'PRODUTO DESCONTINUADO',
            'RUPTURA': 0.002
        },
        {
            'STATUS VENDA': 'PEDIDO DUPLICADO',
            'RUPTURA': 0.001
        },
        {
            'STATUS VENDA': 'QUANTIDADE SOLICITADA INVALIDA',
            'RUPTURA': 0
        },
        {
            'STATUS VENDA': 'VALIDADE CURTA',
            'RUPTURA': 0
        },
        {
            'STATUS VENDA': 'LAYOUT INCORRETO',
            'RUPTURA': 0
        }
    ];

    public rupbynotatendB = [
        {
            'STATUS VENDA': 'FALTA DE ESTOQUE',
            'RUPTURA': 0.003
        },
        {
            'STATUS VENDA': 'CADASTRO (PRODUTO\/CLIENTE) \/ LIMITE DE CREDITO',
            'RUPTURA': 0.005
        },
        {
            'STATUS VENDA': 'PEDIDO MINIMO',
            'RUPTURA': 0.001
        },
        {
            'STATUS VENDA': 'DOCUMENTACAO',
            'RUPTURA': 0.041
        },
        {
            'STATUS VENDA': 'MOTIVO NAO INFORMADO',
            'RUPTURA': 0.942
        },
        {
            'STATUS VENDA': 'PRODUTO DESCONTINUADO',
            'RUPTURA': 0.002
        },
        {
            'STATUS VENDA': 'PEDIDO DUPLICADO',
            'RUPTURA': 0.008
        },
        {
            'STATUS VENDA': 'QUANTIDADE SOLICITADA INVALIDA',
            'RUPTURA': 0
        },
        {
            'STATUS VENDA': 'VALIDADE CURTA',
            'RUPTURA': 0
        },
        {
            'STATUS VENDA': 'LAYOUT INCORRETO',
            'RUPTURA': 0
        }
    ];


    public fillrateByBUA = [
        {
            'MES': 'January',
            'FILLRATE': 0.975230598322299,
            'BU': 'BGX'
        },
        {
            'MES': 'January',
            'FILLRATE': 0.870798878859399,
            'BU': 'CHC'
        },
        {
            'MES': 'January',
            'FILLRATE': 0.949714487710999,
            'BU': 'GENERICO'
        },
        {
            'MES': 'January',
            'FILLRATE': 0.946996997420584,
            'BU': 'PHARMA'
        },
        {
            'MES': 'February',
            'FILLRATE': 0.889555566565922,
            'BU': 'BGX'
        },
        {
            'MES': 'February',
            'FILLRATE': 0.890167423304695,
            'BU': 'CHC'
        },
        {
            'MES': 'February',
            'FILLRATE': 0.938154105359219,
            'BU': 'GENERICO'
        },
        {
            'MES': 'February',
            'FILLRATE': 0.941814925122203,
            'BU': 'PHARMA'
        },
        {
            'MES': 'March',
            'FILLRATE': 0.882335272874596,
            'BU': 'BGX'
        },
        {
            'MES': 'March',
            'FILLRATE': 0.749484735292862,
            'BU': 'CHC'
        },
        {
            'MES': 'March',
            'FILLRATE': 0.685895580711064,
            'BU': 'GENERICO'
        },
        {
            'MES': 'March',
            'FILLRATE': 0.889865680188889,
            'BU': 'PHARMA'
        },
        {
            'MES': 'April',
            'FILLRATE': 0.993997818620311,
            'BU': 'BGX'
        },
        {
            'MES': 'April',
            'FILLRATE': 0.805686244231948,
            'BU': 'CHC'
        },
        {
            'MES': 'April',
            'FILLRATE': 0.758732941002402,
            'BU': 'GENERICO'
        },
        {
            'MES': 'April',
            'FILLRATE': 0.849387683021344,
            'BU': 'PHARMA'
        }
    ];

    public fillrateByBUB = [
        {
            'MES': 'January',
            'FILLRATE': 0.685895580711064,
            'BU': 'BGX'
        },
        {
            'MES': 'January',
            'FILLRATE': 0.870798878859399,
            'BU': 'CHC'
        },
        {
            'MES': 'January',
            'FILLRATE': 0.949714487710999,
            'BU': 'GENERICO'
        },
        {
            'MES': 'January',
            'FILLRATE': 0.946996997420584,
            'BU': 'PHARMA'
        },
        {
            'MES': 'February',
            'FILLRATE': 0.889555566565922,
            'BU': 'BGX'
        },
        {
            'MES': 'February',
            'FILLRATE': 0.890167423304695,
            'BU': 'CHC'
        },
        {
            'MES': 'February',
            'FILLRATE': 0.938154105359219,
            'BU': 'GENERICO'
        },
        {
            'MES': 'February',
            'FILLRATE': 0.749484735292862,
            'BU': 'PHARMA'
        },
        {
            'MES': 'March',
            'FILLRATE': 0.882335272874596,
            'BU': 'BGX'
        },
        {
            'MES': 'March',
            'FILLRATE': 0.941814925122203,
            'BU': 'CHC'
        },
        {
            'MES': 'March',
            'FILLRATE': 0.975230598322299,
            'BU': 'GENERICO'
        },
        {
            'MES': 'March',
            'FILLRATE': 0.889865680188889,
            'BU': 'PHARMA'
        },
        {
            'MES': 'April',
            'FILLRATE': 0.758732941002402,
            'BU': 'BGX'
        },
        {
            'MES': 'April',
            'FILLRATE': 0.805686244231948,
            'BU': 'CHC'
        },
        {
            'MES': 'April',
            'FILLRATE': 0.993997818620311,
            'BU': 'GENERICO'
        },
        {
            'MES': 'April',
            'FILLRATE': 0.849387683021344,
            'BU': 'PHARMA'
        }
    ];


    public fillrateByDayA = [
        {
            'DT_ORDEM': '2018-04-02',
            'QT_ORDEM': 1.079
        },
        {
            'DT_ORDEM': '2018-04-03',
            'QT_ORDEM': 1.14
        },
        {
            'DT_ORDEM': '2018-04-04',
            'QT_ORDEM': 0.577
        },
        {
            'DT_ORDEM': '2018-04-05',
            'QT_ORDEM': 0.711
        },
        {
            'DT_ORDEM': '2018-04-06',
            'QT_ORDEM': 1.602
        },
        {
            'DT_ORDEM': '2018-04-07',
            'QT_ORDEM': 0.007
        },
        {
            'DT_ORDEM': '2018-04-08',
            'QT_ORDEM': 0
        },
        {
            'DT_ORDEM': '2018-04-09',
            'QT_ORDEM': 2.623
        },
        {
            'DT_ORDEM': '2018-04-10',
            'QT_ORDEM': 1.422
        },
        {
            'DT_ORDEM': '2018-04-11',
            'QT_ORDEM': 1.054
        },
        {
            'DT_ORDEM': '2018-04-12',
            'QT_ORDEM': 1.106
        },
        {
            'DT_ORDEM': '2018-04-13',
            'QT_ORDEM': 2.403
        },
        {
            'DT_ORDEM': '2018-04-14',
            'QT_ORDEM': 0.025
        },
        {
            'DT_ORDEM': '2018-04-15',
            'QT_ORDEM': 0.007
        },
        {
            'DT_ORDEM': '2018-04-16',
            'QT_ORDEM': 0.974
        },
        {
            'DT_ORDEM': '2018-04-17',
            'QT_ORDEM': 0.593
        },
        {
            'DT_ORDEM': '2018-04-18',
            'QT_ORDEM': 0.671
        },
        {
            'DT_ORDEM': '2018-04-19',
            'QT_ORDEM': 0.197
        },
        {
            'DT_ORDEM': '2018-04-20',
            'QT_ORDEM': 0.007
        }
    ];

    public leadtimeByMonthA = [
        {
            'MES': 'November',
            'ATE_FATURAMENTO': 5,
            'ATE_ENTREGA': 7
        },
        {
            'MES': 'December',
            'ATE_FATURAMENTO': 8,
            'ATE_ENTREGA': 9.1
        },
        {
            'MES': 'January',
            'ATE_FATURAMENTO': 5.5,
            'ATE_ENTREGA': 7.2
        },
        {
            'MES': 'February',
            'ATE_FATURAMENTO': 5.9,
            'ATE_ENTREGA': 6.1
        },
        {
            'MES': 'March',
            'ATE_FATURAMENTO': 6.9,
            'ATE_ENTREGA': 6.7
        },
        {
            'MES': 'April',
            'ATE_FATURAMENTO': 2.1,
            'ATE_ENTREGA': 4.2
        }
    ];

    public leadtimeByRegionA = [
        {
            'MES': 'January',
            'REGIAO': 'Centro-Oeste',
            'ATE_FATURAMENTO': 6.2,
            'ATE_ENTREGA': 8
        },
        {
            'MES': 'January',
            'REGIAO': 'Nordeste',
            'ATE_FATURAMENTO': 5.9,
            'ATE_ENTREGA': 10.7
        },
        {
            'MES': 'January',
            'REGIAO': 'Norte',
            'ATE_FATURAMENTO': 11.9,
            'ATE_ENTREGA': 11.7
        },
        {
            'MES': 'January',
            'REGIAO': 'Sudeste',
            'ATE_FATURAMENTO': 4.9,
            'ATE_ENTREGA': 5.8
        },
        {
            'MES': 'January',
            'REGIAO': 'Sul',
            'ATE_FATURAMENTO': 6.8,
            'ATE_ENTREGA': 6.6
        }
    ];

    public leadtimeByStatusA = [
        {
            'MES': 'January',
            'DS_STATUS_ENTREGA': 'DEVOLUCAO',
            'VALOR': 0.002
        },
        {
            'MES': 'January',
            'DS_STATUS_ENTREGA': 'ENTREGUE',
            'VALOR': 0.998
        }
    ];
}
