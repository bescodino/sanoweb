import { IParserDataIdentifiers, IFactoryID } from '../../charts/chart-entities.model';

export class DistributorIdentifier implements IParserDataIdentifiers {

    lblFirst = 'DISTRIBUIDOR';
    lblSecond = 'RUPTURA';

    getLabels(): string[] {
        return [this.lblFirst, this.lblSecond];
    }

    // getAggregator() {
    //     throw new Error('Method not implemented.');
    // }

    // getLabel(): string {
    //     return this.lblKey;
    // }

    // getData(): string {
    //     return this.lblValue;
    // }
}

export class SalesStatusIdentifier implements IParserDataIdentifiers {

    lblFirst = 'STATUS VENDA';
    lblSecond = 'RUPTURA';

    getLabels(): string[] {
        return [this.lblFirst, this.lblSecond];
    }

    // getAggregator() {
    //     throw new Error('Method not implemented.');
    // }

    // getLabel(): string {
    //     return this.lblKey;
    // }

    // getData(): string {
    //     return this.lblValue;
    // }
}

export class AggregateMonthlyIdentifier implements IParserDataIdentifiers {

    lblFirst = 'MES';
    lblSecond = 'BU';
    lblThird = 'FILLRATE';

    getLabels(): string[] {
        return [this.lblFirst, this.lblSecond, this.lblThird];
    }

    // getAggregator() {
    //     return this.lblAggregator;
    // }

    // getLabel(): string {
    //     return this.lblKey;
    // }

    // getData(): string {
    //     return this.lblValue;
    // }
}

export class DailyIdentifier implements IParserDataIdentifiers {

    lblFirst = 'DT_ORDEM';
    lblSecond = 'QT_ORDEM';

    getLabels(): string[] {
        return [this.lblFirst, this.lblSecond];
    }

    // getAggregator() {
    //     throw new Error('Method not implemented.');
    // }

    // getLabel(): string {
    //     return this.lblKey;
    // }

    // getData(): string {
    //     return this.lblValue;
    // }
}

export class LeadtimeByMonth implements IParserDataIdentifiers {

    lblFirst = 'MES';
    lblSecond = 'ATE_FATURAMENTO';
    lblThird = 'ATE_ENTREGA';


    getLabels(): string[] {
        return [this.lblFirst, this.lblSecond, this.lblThird];
    }

    getAggregator() {
        throw new Error('Method not implemented.');
    }
    getLabel() {
        throw new Error('Method not implemented.');
    }
    getData() {
        throw new Error('Method not implemented.');
    }
}


export class LeadtimeByRegion implements IParserDataIdentifiers {

    lblFirst = 'MES';
    lblSecond = 'REGIAO';
    lblThird = 'ATE_FATURAMENTO';
    lblFourth = 'ATE_ENTREGA';


    getLabels(): string[] {
        return [this.lblFirst, this.lblSecond, this.lblThird, this.lblFourth];
    }

    getAggregator() {
        throw new Error('Method not implemented.');
    }
    getLabel() {
        throw new Error('Method not implemented.');
    }
    getData() {
        throw new Error('Method not implemented.');
    }
}

export class LeadtimeByStatus implements IParserDataIdentifiers {

    lblFirst = 'DS_STATUS_ENTREGA';
    lblSecond = 'VALOR';
    lblThird = 'MES';

    getLabels(): string[] {
        return [this.lblFirst, this.lblSecond, this.lblThird];
    }

    getAggregator() {
        throw new Error('Method not implemented.');
    }
    getLabel() {
        throw new Error('Method not implemented.');
    }
    getData() {
        throw new Error('Method not implemented.');
    }
}
