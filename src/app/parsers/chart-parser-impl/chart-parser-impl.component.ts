import { IChartParser } from '../../charts/chart-entities.model';
import { ApiSettings } from '../../settings/api.settings';
import { IChartContentNEW, IChartEntryNEW, IParserDataIdentifiers, IFactoryID } from '../../charts/chart-entities.model';
import { ChartCommonEntry } from '../../charts/charts-implementation';

import { EntryFactoryService } from '../../factories/entry/entry-factory.service';
import { DataIdentifiersFactory } from '../../factories/data-identifier/data-identifiers.service';
import { ContentFactoryService } from '../../factories/content/content-factory.service';

import { TranslateService } from '@ngx-translate/core';
import { del } from 'selenium-webdriver/http';

class ChartParserEntity implements IChartParser, IFactoryID {

    factoryID: string;

    constructor(factoryID: string) {
        this.set(factoryID);
    }

    set(factoryID: string) {
        this.factoryID = factoryID;
    }

    parse(responseData: any, sort: boolean): IChartContentNEW {
        throw new Error('Method should not be called;');
    }

    sort(elementA, elementB, label: string) {
        if (elementA[label] > elementB[label]) {
            return -1;
        }

        if (elementA[label] < elementB[label]) {
            return 1;
        }
        // lblKey: string;

        return 0;
    }
}

/**
 * Simple Key, Value parser
 */
export class ChartCommonParser extends ChartParserEntity {

    factoryID: string;

    lblKey: string;
    lblValue: string;

    constructor(factoryID: string,
        private entryFactoryService: EntryFactoryService,
        private contentFactoryService: ContentFactoryService,
        private dataIdentifiersFactory: DataIdentifiersFactory) {

        super(factoryID);

        const dataID = this.dataIdentifiersFactory
            .getDataIdentifier(this.factoryID)
            .getLabels();

        this.lblKey = dataID[0];
        this.lblValue = dataID[1];
    }

    parse(responseData: any, sort: boolean): IChartContentNEW {
        // console.log('parse::responseData::starting', JSON.stringify(responseData));

        let rawData = responseData;

        if (sort) { rawData = rawData.sort((a, b) => this.sort(a, b, 'RUPTURA')); };

        const content = this.contentFactoryService.getContent(this.factoryID);

        rawData
            .forEach(element => {
                // console.log('parse::element:', JSON.stringify(element));

                const entry = this.entryFactoryService
                    .getEntry(this.factoryID)
                    .make(element[this.lblKey], element[this.lblValue]);

                content.addEntry(entry);
            });

        content.print();
        return content;
    }


}


export class ChartTranslateKeyParser extends ChartParserEntity {

    factoryID: string;

    lblKey: string;
    lblValue: string;

    constructor(factoryID: string,
        private entryFactoryService: EntryFactoryService,
        private contentFactoryService: ContentFactoryService,
        private dataIdentifiersFactory: DataIdentifiersFactory,
        private translate: TranslateService) {

        super(factoryID);

        const dataID = this.dataIdentifiersFactory
            .getDataIdentifier(this.factoryID)
            .getLabels();

        this.lblKey = dataID[0];
        this.lblValue = dataID[1];
    }

    /**
     * Traduz a chave recebida
     * @param responseData
     * @param sort
     */
    parse(responseData: any, sort: boolean): IChartContentNEW {
        console.log('parse::responseData::starting', JSON.stringify(responseData));

        const rawData = responseData;

        // if (sort) { rawData = rawData.sort((a, b) => this.sort(a, b)); };

        const content = this.contentFactoryService.getContent(this.factoryID);

        rawData
            .forEach(element => {
                console.log('translated: ', JSON.stringify(this.translate.instant('sales.status.' + element[this.lblKey])));
                console.log('translated: ', JSON.stringify(this.translate.instant('osa.chains.basket')));

                const entry = this.entryFactoryService
                    .getEntry(this.factoryID)
                    .make(this.translate.get('sales.status.' + element[this.lblKey]), element[this.lblValue]);

                content.addEntry(entry);
            });

        content.print();
        return content;
    }

}

export class ChartAggregateMonthsParser extends ChartParserEntity {
    factoryID: string;

    lblAggregator: string;
    lblKey: string;
    lblValue: string;

    constructor(factoryID: string,
        private entryFactoryService: EntryFactoryService,
        private contentFactoryService: ContentFactoryService,
        private dataIdentifiersFactory: DataIdentifiersFactory) {

        super(factoryID);

        const dataID = this.dataIdentifiersFactory
            .getDataIdentifier(this.factoryID)
            .getLabels();

        this.lblAggregator = dataID[0];
        this.lblKey = dataID[1];
        this.lblValue = dataID[2];
    }

    /**
     * Traduz a chave recebida
     * @param responseData
     * @param sort
     */
    parse(responseData: any, sort: boolean): IChartContentNEW {
        console.log('parse::responseData::starting', JSON.stringify(responseData));

        const rawData: Array<any> = responseData;

        // if (sort) { rawData = rawData.sort((a, b) => this.sort(a, b)); };

        const months = new Array<string>();
        const labs = new Array<string>();
        const labsData: { [lab: string]: Array<string>; } = {};

        rawData
            .map(value => {
                if (!months.includes(value[this.lblAggregator])) { months.push(value[this.lblAggregator]); }
                if (!labs.includes(value[this.lblKey])) { labs.push(value[this.lblKey]); }
            });

        for (let i = 0; i < labs.length; i++) {

            const labName = labs[i];
            rawData
                .filter(value => {
                    return value[this.lblKey] === labName;
                })
                .map(value => {

                    if (!labsData[labName]) {
                        labsData[labName] = [value[this.lblValue]];
                    } else {
                        // Funny workaround code
                        const hackyMchackerson = labsData[labName];
                        hackyMchackerson.push(value[this.lblValue] as string);
                        labsData[labName] = hackyMchackerson;
                    }
                });
        }

        // console.log('Labels', JSON.stringify(months));
        // console.log('Entries::', JSON.stringify(labsData));

        const content = this.contentFactoryService.getContent(this.factoryID);
        content.setLabels(months);

        for (const lab in labsData) {
            if (labsData.hasOwnProperty(lab)) {
                const data = labsData[lab];
                const entry = this.entryFactoryService
                    .getEntry(this.factoryID)
                    .make(lab, data);

                content.addEntry(entry);
            }
        }

        content.print();
        return content;
    }
}

export class ChartDailyParser extends ChartParserEntity {

    factoryID: string;

    lblKey: string;
    lblValue: string;

    days: string[];
    values: string[];

    constructor(factoryID: string,
        private entryFactoryService: EntryFactoryService,
        private contentFactoryService: ContentFactoryService,
        private dataIdentifiersFactory: DataIdentifiersFactory) {

        super(factoryID);

        const dataID = this.dataIdentifiersFactory
            .getDataIdentifier(this.factoryID)
            .getLabels();

        this.lblKey = dataID[0];
        this.lblValue = dataID[1];
    }

    parse(responseData: any, sort: boolean): IChartContentNEW {
        // console.log('parse::responseData::starting', JSON.stringify(responseData));

        const rawData = responseData;

        // if (sort) { rawData = rawData.sort((a, b) => this.sort(a, b, 'RUPTURA')); };

        const content = this.contentFactoryService.getContent(this.factoryID);

        rawData
            .forEach(element => {
                // console.log('parse::element:', JSON.stringify(element));

                const entry = this.entryFactoryService
                    .getEntry(this.factoryID)
                    .make(element[this.lblKey], element[this.lblValue]);

                content.addEntry(entry);
            });

        content.print();
        return content;
    }


}


export class ChartLeadtimeByMonthParser extends ChartParserEntity {

    lblFirst: string;
    lblSecond: string;
    lblThird: string;

    constructor(factoryID: string,
        private entryFactoryService: EntryFactoryService,
        private contentFactoryService: ContentFactoryService,
        private dataIdentifiersFactory: DataIdentifiersFactory) {

        super(factoryID);

        const dataID = this.dataIdentifiersFactory
            .getDataIdentifier(this.factoryID)
            .getLabels();

        this.lblFirst = dataID[0];
        this.lblSecond = dataID[1];
        this.lblThird = dataID[2];
    }

    /**
     * Traduz a chave recebida
     * @param responseData
     * @param sort
     */
    parse(responseData: any, sort: boolean): IChartContentNEW {
        console.log('parse::responseData::starting', JSON.stringify(responseData));

        const rawData: Array<any> = responseData;

        // if (sort) { rawData = rawData.sort((a, b) => this.sort(a, b)); };

        const months = new Array<string>();
        const upToBilling = new Array<string>();
        const upToDelivery = new Array<string>();

        rawData
            .map(value => {
                if (!months.includes(value[this.lblFirst])) { months.push(value[this.lblFirst]); }
                if (!upToBilling.includes(value[this.lblSecond])) { upToBilling.push(value[this.lblSecond]); }
                if (!upToDelivery.includes(value[this.lblThird])) { upToDelivery.push(value[this.lblThird]); }
            });


        console.log('months', JSON.stringify(months));
        console.log('upToBilling::', JSON.stringify(upToBilling));
        console.log('upToDelivery::', JSON.stringify(upToDelivery));

        const content = this.contentFactoryService.getContent(this.factoryID);
        content.setLabels(months);

        const billing = this.entryFactoryService
            .getEntry(this.factoryID)
            .make('Up to Billing', upToBilling);

        const delivery = this.entryFactoryService
            .getEntry(this.factoryID)
            .make('Up to Delivery', upToDelivery);

        content.addEntry(billing);
        content.addEntry(delivery);

        content.print();
        return content;
    }

}

export class ChartLeadtimeByRegionParser extends ChartParserEntity {

    lblFirst: string;
    lblSecond: string;
    lblThird: string;

    constructor(factoryID: string,
        private entryFactoryService: EntryFactoryService,
        private contentFactoryService: ContentFactoryService,
        private dataIdentifiersFactory: DataIdentifiersFactory) {

        super(factoryID);

        const dataID = this.dataIdentifiersFactory
            .getDataIdentifier(this.factoryID)
            .getLabels();

        // Ignorando a label para mês
        this.lblFirst = dataID[1];
        this.lblSecond = dataID[2];
        this.lblThird = dataID[3];
    }

    /**
     * Traduz a chave recebida
     * @param responseData
     * @param sort
     */
    parse(responseData: any, sort: boolean): IChartContentNEW {
        console.log('parse::responseData::starting', JSON.stringify(responseData));

        const rawData: Array<any> = responseData;

        // if (sort) { rawData = rawData.sort((a, b) => this.sort(a, b)); };

        const regions = new Array<string>();
        const upToBilling = new Array<string>();
        const upToDelivery = new Array<string>();

        rawData
            .map(value => {
                if (!regions.includes(value[this.lblFirst])) { regions.push(value[this.lblFirst]); }
                if (!upToBilling.includes(value[this.lblSecond])) { upToBilling.push(value[this.lblSecond]); }
                if (!upToDelivery.includes(value[this.lblThird])) { upToDelivery.push(value[this.lblThird]); }
            });


        console.log('regions', JSON.stringify(regions));
        console.log('upToBilling::', JSON.stringify(upToBilling));
        console.log('upToDelivery::', JSON.stringify(upToDelivery));

        const content = this.contentFactoryService.getContent(this.factoryID);
        content.setLabels(regions);

        const billing = this.entryFactoryService
            .getEntry(this.factoryID)
            .make('Up to Billing', upToBilling);

        const delivery = this.entryFactoryService
            .getEntry(this.factoryID)
            .make('Up to Delivery', upToDelivery);

        content.addEntry(billing);
        content.addEntry(delivery);

        content.print();
        return content;
    }

}
