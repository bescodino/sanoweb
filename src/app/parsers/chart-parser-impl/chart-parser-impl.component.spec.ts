import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartParserImplComponent } from './chart-parser-impl.component';

describe('ChartParserImplComponent', () => {
  let component: ChartParserImplComponent;
  let fixture: ComponentFixture<ChartParserImplComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartParserImplComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartParserImplComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
