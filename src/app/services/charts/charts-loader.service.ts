import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { IChart } from '../../charts/chart-entities.model';
import { MOCK } from '../../MOCKS/MOCKS';

@Injectable()
export class ChartsLoaderService<T extends IChart>{

    static readonly headers = new Headers({ 'Content-Type': 'application/json' });
    private mock = new MOCK();

    constructor(
        protected http: Http) {
    }

    get(baseUrl: string, filterQuery?: string): Promise<any> {
        const url = baseUrl + (filterQuery || '');
        console.log('get::', url);

        return this.http.get(url)
            .toPromise()
            .then(response => {
                // console.log('this.http.get::' + url + '::returned', response);
                return response.json();
            })
            .catch(response => { return this.handleError(response); });
    }

    getAsObservable(baseUrl: string, filterQuery?: string, type?: string): Observable<any> {
        const url = baseUrl + (filterQuery || '');
        console.log('getAsObservable::url:', url);
        console.log('getAsObservable::type:', type);

        return this.http
            .get(url)
            .map(res => res.json());
    }

    getAsObservableMOCK(baseUrl: string, type: string, filterQuery?: string): Observable<any> {
        const url = baseUrl + (filterQuery || '');
        console.log('getAsObservableMOCK::url:', url);
        // console.log('getAsObservableMOCK::type:', type);

        return Observable.of(this.mock[type]);
    }


    getAll(baseUrl: string): Promise<T[]> {
        return this.http.get(baseUrl)
            .toPromise()
            .then(response => {
                // console.log('getAll::', JSON.stringify(response.json()));
                return response.json() as T[];
            })
            .catch(this.handleError);
    }

    getByUuid(baseUrl: string, uuid: string): Promise<T> {
        const url = `${baseUrl}/${uuid}`;
        return this.http.get(url)
            .toPromise()
            .then(response => response.json().data as T)
            .catch(this.handleError);
    }


    handleError(response: any): Promise<any> {
        console.error('An error occurred', response);
        return Promise.reject(response.json().errors || response);
    }
}
