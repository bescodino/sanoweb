import { Injectable, EventEmitter } from '@angular/core';
import * as Chart from 'chart.js';

@Injectable()
export class ChartsEventEmitterService {

    public graphClicked$: EventEmitter<{ item: any, graphLabel: string }>;


    constructor() {
        this.graphClicked$ = new EventEmitter();
    }


    public emitClick(item: any, graphLabel: string): void {

        if (!item || !item.length || item.length <= 0) { return; }

        this.graphClicked$.emit({ item, graphLabel });
    }

}
