import { TestBed, inject } from '@angular/core/testing';

import { ChartsLoaderService } from './charts-loader.service';
import { IChart } from '../../shared/chart-entity.model';

describe('ChartsLoaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChartsLoaderService]
    });
  });

  it('should ...', inject([ChartsLoaderService], (service: ChartsLoaderService<IChart>) => {
    expect(service).toBeTruthy();
  }));
});
