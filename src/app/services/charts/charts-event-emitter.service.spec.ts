import { TestBed, inject } from '@angular/core/testing';

import { ChartsEventEmitterService } from './charts-event-emitter.service';

describe('ChartEventEmitterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChartsEventEmitterService]
    });
  });

  it('should ...', inject([ChartsEventEmitterService], (service: ChartsEventEmitterService) => {
    expect(service).toBeTruthy();
  }));
});
