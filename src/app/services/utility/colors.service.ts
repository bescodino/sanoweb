import { Injectable } from '@angular/core';

@Injectable()
export class ColorsService {

    colorCodes = {
        aqua: '#00ffff',
        azure: '#f0ffff',
        beige: '#f5f5dc',
        black: '#000000',
        blue: '#0000ff',
        brown: '#a52a2a',
        cyan: '#00ffff',
        darkblue: '#00008b',
        darkcyan: '#008b8b',
        darkgrey: '#a9a9a9',
        darkgreen: '#006400',
        darkkhaki: '#bdb76b',
        darkmagenta: '#8b008b',
        darkorange: '#ff8c00',
        darkorchid: '#9932cc',
        darkred: '#8b0000',
        darksalmon: '#e9967a',
        darkviolet: '#9400d3',
        fuchsia: '#ff00ff',
        gold: '#ffd700',
        green: '#008000',
        indigo: '#4b0082',
        khaki: '#f0e68c',
        lightblue: '#add8e6',
        lightcyan: '#e0ffff',
        lightgreen: '#90ee90',
        lightgrey: '#d3d3d3',
        lightpink: '#ffb6c1',
        lightyellow: '#ffffe0',
        lime: '#00ff00',
        magenta: '#ff00ff',
        maroon: '#800000',
        navy: '#000080',
        olive: '#808000',
        orange: '#ffa500',
        pink: '#ffc0cb',
        purple: '#800080',
        violet: '#800080',
        red: '#ff0000',
        silver: '#c0c0c0',
        yellow: '#ffff00',
        green20: '#6cb67d',
        green50: '#449056'
    };

    colorNames = [
        'aqua',
        'azure',
        'beige',
        'black',
        'blue',
        'brown',
        'cyan',
        'darkblue',
        'darkcyan',
        'darkgrey',
        'darkgreen',
        'darkkhaki',
        'darkmagenta',
        'darkorange',
        'darkorchid',
        'darkred',
        'darksalmon',
        'darkviolet',
        'fuchsia',
        'gold',
        'green',
        'indigo',
        'khaki',
        'lightblue',
        'lightcyan',
        'lightgreen',
        'lightgrey',
        'lightpink',
        'lightyellow',
        'lime',
        'magenta',
        'maroon',
        'navy',
        'olive',
        'orange',
        'pink',
        'purple',
        'violet',
        'red',
        'silver',
        'yellow',
        'green20',
        'green50',
    ];


    public randomName(): string {
        const color = this.colorNames[Math.floor(Math.random() * this.colorNames.length) + 1];

        // console.log('color::', JSON.stringify(color));
        // console.log('code::', JSON.stringify(this.colorCodes[color]));

        return color;
    };

    public fixColor(): string[] {

      return [
          'blue',
          'green',
          'yellow',
          'white',
        ];
    };

    public fixColorBarras(): string[] {

      return [
        '#569d57',
        '#466f47',
        ];
    };

}
