import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';


@Injectable()
export class MonthsService {

    private months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ];

    constructor() { }

    getAll(): string[] {
        return this.months;
    }

    getByUuid(index: number): string {
        return this.months[index - 1];
    }

    getLabel(arrayIndex: number): number {
        return arrayIndex + 1;
    }

}
