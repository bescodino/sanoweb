import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent {

  state = false;

  constructor() { }


  show() {
    this.state = true;
  }

  hide() {
    this.state = false;
  }

  flip() {
    this.state = !this.state;
  }

}
