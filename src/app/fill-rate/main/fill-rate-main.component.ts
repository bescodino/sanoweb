import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

import * as Chart from 'chart.js';

// RxJS
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

// Creator Patterns
import 'rxjs/add/observable/fromEvent';

// Transformation Methods
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/take';

// Constant Settings
import { ApiSettings } from '../../settings/api.settings';
import { FactoryMapper } from '../../settings/factory.mapper';

import { ChartsLoaderService } from '../../services/charts/charts-loader.service';
import { ChartsEventEmitterService } from '../../services/charts/charts-event-emitter.service';
import { IChart, IChartParser, IChartNEW, IChartContentNEW } from '../../charts/chart-entities.model';

// Factories
import { ChartsParserFactory } from '../../factories/parser/parser-factory.service';
import { ChartsFactory } from '../../factories/chart/chart-factory.service';
import { ChartsMetadataFactory } from '../../factories/chart/chart-metadata-factory.service';

import { FilterQuery } from '../../filter/filter-query.component';


@Component({
    selector: 'app-fill-rate-main',
    templateUrl: './fill-rate-main.component.html',
    styleUrls: ['./fill-rate-main.component.css']
})
export class FillRateMainComponent implements OnInit, AfterViewInit {

    private settings = new ApiSettings();
    private factoryMapper = new FactoryMapper();

    public readonly serviceByBuCanvasID = 'service-by-bu';
    public serviceByBu$: Observable<any>;
    public updServiceByBu$: Observable<any>;
    public serviceByBuChart: IChartNEW;
    public serviceByBuInnerChart: Chart;

    public readonly fillrateByDayCanvasID = 'fillrate-by-day';
    public fillrateByDay$: Observable<any>;
    public updfillrateByDay$: Observable<any>;
    public fillrateByDayChart: IChartNEW;
    public fillrateByDayInnerChart: Chart;

    public readonly leadtimeByMonthCanvasID = 'leadtime-by-month';
    public leadtimeByMonth$: Observable<any>;
    public updLeadtimeByMonth$: Observable<any>;
    public leadtimeByMonthChart: IChartNEW;
    public leadtimeByMonthInnerChart: Chart;

    public readonly leadtimeByRegionCanvasID = 'leadtime-by-region';
    public leadtimeByRegion$: Observable<any>;
    public updLeadtimeByRegion$: Observable<any>;
    public leadtimeByRegionChart: IChartNEW;
    public leadtimeByRegionInnerChart: Chart;


    public readonly leadtimeByStatusCanvasID = 'leadtime-by-status';
    public leadtimeByStatus$: Observable<any>;
    public updleadtimeByStatus$: Observable<any>;
    public leadtimeByStatusChart: IChartNEW;
    public leadtimeByStatusInnerChart: Chart;

    private subscriptions$: Array<Observable<any>>;
    private end$ = this.router.events.filter((event) => event instanceof NavigationEnd);


    constructor(
        private chartsLoaderService: ChartsLoaderService<IChart>,
        private chartsParserFactory: ChartsParserFactory,
        private chartsMetadataFactory: ChartsMetadataFactory,
        private chartsFactory: ChartsFactory,
        private eventEmitter: ChartsEventEmitterService,
        private router: Router) {

        // this.filterQuery = new FilterQuery();
        this.subscriptions$ = new Array<Observable<any>>();

        eventEmitter.graphClicked$
            .subscribe((item) => this.onChartClicked(item));

    }


    ngOnInit() {
        this.loadCharts();
    }

    ngAfterViewInit() {
        this.subscribeCharts();
    }

    onChartClicked(item: any) {
        // this.filterQuery.toggle(item.graphLabel, item.item[0]._model.label);
        // console.log('chartClicked::query::', JSON.stringify(this.filterQuery.getQuery()));
        console.log('chartClicked::item::', item);
    }


    loadCharts() {
        this.subscriptions$.push(
            // Atendimento por BU
            this.chartsLoaderService
                // .getAsObservableMOCK(this.settings.fillrateByBU, 'fillrateByBUA')
                .getAsObservable(this.settings.fillrateByBU)
                .takeUntil(this.end$)
                .take(1)
                .map(res => {
                    const factoryID = this.factoryMapper.getId(this.settings.fillrateByBU);
                    console.log('loadCharts::factoryID::', JSON.stringify(factoryID));

                    // Parsing raw response to a Content
                    const content: IChartContentNEW = this.chartsParserFactory
                        .getParser(factoryID)
                        .parse(res, false);

                    // Getting existing or new Chart
                    this.serviceByBuChart = this.serviceByBuChart || this.chartsFactory.getChart(factoryID);

                    // Populating all data
                    this.serviceByBuChart
                        .setContent(content)
                        .setMetaDataContent(this.chartsMetadataFactory.getMetadata(factoryID));

                    // Rendering and keeping reference
                    this.serviceByBuInnerChart = this.serviceByBuChart.renderChart(this.serviceByBuCanvasID);
                }),

            // Entrada de Pedidos
            this.chartsLoaderService
                // .getAsObservable(this.settings.fillrateByDay)
                // .getAsObservableMOCK(this.settings.fillrateByDay, 'fillrateByDayA')
                .getAsObservable(this.settings.fillrateByDay)
                .takeUntil(this.end$)
                .take(1)
                .map(res => {
                    const factoryID = this.factoryMapper.getId(this.settings.fillrateByDay);
                    console.log('loadCharts::factoryID::', JSON.stringify(factoryID));

                    // Parsing raw response to a Content
                    const content: IChartContentNEW = this.chartsParserFactory
                        .getParser(factoryID)
                        .parse(res, false);

                    // Getting existing or new Chart
                    this.fillrateByDayChart = this.fillrateByDayChart || this.chartsFactory.getChart(factoryID);

                    // Populating all data
                    this.fillrateByDayChart
                        .setContent(content)
                        .setMetaDataContent(this.chartsMetadataFactory.getMetadata(factoryID));

                    // Rendering and keeping reference
                    this.fillrateByDayInnerChart = this.fillrateByDayChart.renderChart(this.fillrateByDayCanvasID);
                }),

            // Leadtime por mês
            this.chartsLoaderService
                // .getAsObservableMOCK(this.settings.leadtimeByMonth, 'leadtimeByMonthA')
                .getAsObservable(this.settings.leadtimeByMonth)
                .takeUntil(this.end$)
                .take(1)
                .map(res => {
                    const factoryID = this.factoryMapper.getId(this.settings.leadtimeByMonth);
                    console.log('loadCharts::factoryID::', JSON.stringify(factoryID));

                    // Parsing raw response to a Content
                    const content: IChartContentNEW = this.chartsParserFactory
                        .getParser(factoryID)
                        .parse(res, false);

                    // Getting existing or new Chart
                    this.leadtimeByMonthChart = this.leadtimeByMonthChart || this.chartsFactory.getChart(factoryID);

                    // Populating all data
                    this.leadtimeByMonthChart
                        .setContent(content)
                        .setMetaDataContent(this.chartsMetadataFactory.getMetadata(factoryID));

                    // Rendering and keeping reference
                    this.leadtimeByMonthInnerChart = this.leadtimeByMonthChart.renderChart(this.leadtimeByMonthCanvasID);
                }),

            // Leadtime por regiao
            this.chartsLoaderService
                // .getAsObservableMOCK(this.settings.leadtimeByRegion, 'leadtimeByRegionA')
                .getAsObservable(this.settings.leadtimeByRegion)
                .takeUntil(this.end$)
                .take(1)
                .map(res => {
                    const factoryID = this.factoryMapper.getId(this.settings.leadtimeByRegion);
                    console.log('loadCharts::factoryID::', JSON.stringify(factoryID));

                    // Parsing raw response to a Content
                    const content: IChartContentNEW = this.chartsParserFactory
                        .getParser(factoryID)
                        .parse(res, false);

                    // Getting existing or new Chart
                    this.leadtimeByRegionChart = this.leadtimeByRegionChart || this.chartsFactory.getChart(factoryID);

                    // Populating all data
                    this.leadtimeByRegionChart
                        .setContent(content)
                        .setMetaDataContent(this.chartsMetadataFactory.getMetadata(factoryID));

                    // Rendering and keeping reference
                    this.leadtimeByRegionInnerChart =
                        this.leadtimeByRegionChart.renderChart(this.leadtimeByRegionCanvasID);
                }),

            // Status da Entrega
            this.chartsLoaderService
                // .getAsObservableMOCK(this.settings.leadtimeByStatus, 'leadtimeByStatusA')
                .getAsObservable(this.settings.leadtimeByStatus)
                .takeUntil(this.end$)
                .take(1)
                .map(res => {
                    const factoryID = this.factoryMapper.getId(this.settings.leadtimeByStatus);
                    console.log('loadCharts::factoryID::', JSON.stringify(factoryID));

                    // Parsing raw response to a Content
                    const content: IChartContentNEW = this.chartsParserFactory
                        .getParser(factoryID)
                        .parse(res, false);

                    // Getting existing or new Chart
                    this.leadtimeByStatusChart = this.leadtimeByStatusChart || this.chartsFactory.getChart(factoryID);

                    // Populating all data
                    this.leadtimeByStatusChart
                        .setContent(content)
                        .setMetaDataContent(this.chartsMetadataFactory.getMetadata(factoryID));

                    // Rendering and keeping reference
                    this.leadtimeByStatusInnerChart =
                        this.leadtimeByStatusChart.renderChart(this.leadtimeByStatusCanvasID);
                }),



        );


    }

    updateCharts() { }

    subscribeCharts() {
        this.subscriptions$.forEach(element => {
            element.subscribe();
        });
    }




}
