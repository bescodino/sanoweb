import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FillRateMainComponent } from './fill-rate-main.component';

describe('FillRateMainComponent', () => {
  let component: FillRateMainComponent;
  let fixture: ComponentFixture<FillRateMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FillRateMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FillRateMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
