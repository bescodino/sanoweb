import { TestBed, inject } from '@angular/core/testing';

import { ChartsFactory } from './chart-factory.service';

describe('ChartsParserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChartsFactory]
    });
  });

  it('should ...', inject([ChartsFactory], (service: ChartsFactory) => {
    expect(service).toBeTruthy();
  }));
});
