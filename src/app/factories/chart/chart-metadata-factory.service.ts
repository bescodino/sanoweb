import { Injectable } from '@angular/core';

// Import Settings/Utilities
import { ApiSettings } from '../../settings/api.settings';
import { FactoryMapper } from '../../settings/factory.mapper';
import { ColorsService } from '../../services/utility/colors.service';

// Import interfaces and implementations
import { IMetadataNEW } from '../../charts/chart-entities.model';
import {
    MetadataDistributor, MetadataSalesStatus,
    MetadataMonthly, MetadataDaily, MetadataStacked, MetadataHorizontal
} from '../../charts/chart-metadata-library';


@Injectable()
export class ChartsMetadataFactory {

    private factoryMapper = new FactoryMapper();
    constructor(private colors: ColorsService) { };

    /**
     * Retorna a Metadata apropriada para a requisição passada.
     * @param chartId Identificação do recurso
     */
    public getMetadata(chartId: string): IMetadataNEW {

        console.log('ChartsMetadataFactory::getMetadata::starting: ', chartId);

        switch (chartId) {

            case this.factoryMapper.salesStatusID:
                console.log('Creating \'Sales Status\' Metadata...');
                return new MetadataSalesStatus();

            case this.factoryMapper.distributorsID:
                console.log('Creating \'Distributor\' Metadata...');
                return new MetadataDistributor();

            case this.factoryMapper.monthlyID:
                console.log('Creating \'Monthly\' Metadata...');
                return new MetadataMonthly(this.colors);

            case this.factoryMapper.dailyID:
                console.log('Creating \'Daily\' Metadata...');
                return new MetadataDaily();

            case this.factoryMapper.leadtimeByRegion:
            case this.factoryMapper.multiDatasetID:
                console.log('Creating \'Stacked\' Metadata...');
                return new MetadataStacked(this.colors);


            case this.factoryMapper.leadtimeByStatus:
                console.log('Creating \'Doughnut\' Metadata...');
                return new MetadataHorizontal();

            default:
                throw new Error('Exception:: no IMetadata could be inferred: bad request ID.');
        }
    }
}
