import { Injectable } from '@angular/core';

// Import Settings
import { ApiSettings } from '../../settings/api.settings';
import { FactoryMapper } from '../../settings/factory.mapper';

// Import interfaces and implementations
import { IChartNEW } from '../../charts/chart-entities.model';
import { ChartCommon, ChartAggregate, ChartDaily, ChartStacked } from '../../charts/charts-implementation';

@Injectable()
export class ChartsFactory {

    private factoryMapper = new FactoryMapper();

    /**
     * Retorna o IChart apropriado para a requisição passada.
     * @param chartId O mesmo parâmetro usado na requisição feita para adquirir o recurso
     */
    public getChart(chartId: string): IChartNEW {

        console.log('ChartsFactory::getChart::starting: ', chartId);

        switch (chartId) {

            case this.factoryMapper.leadtimeByStatus:
            case this.factoryMapper.salesStatusID:
            case this.factoryMapper.distributorsID:
                console.log('Creating \'Common Chart\' Chart...');
                return new ChartCommon();

            case this.factoryMapper.monthlyID:
                console.log('Creating \'Aggregate\' Chart...');
                return new ChartAggregate();

            case this.factoryMapper.dailyID:
                console.log('Creating \'Daily\' Chart...');
                return new ChartDaily();

            case this.factoryMapper.leadtimeByRegion:
            case this.factoryMapper.multiDatasetID:
                console.log('Creating \'Stacked\' Chart...');
                return new ChartStacked();

            default:
                throw new Error('Exception:: no IChart could be inferred: bad request ID.');
        }
    }
}
