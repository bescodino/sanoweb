import { TestBed, inject } from '@angular/core/testing';

import { ChartsMetadataFactory } from './chart-metadata-factory.service';

describe('ChartsParserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChartsMetadataFactory]
    });
  });

  it('should ...', inject([ChartsMetadataFactory], (service: ChartsMetadataFactory) => {
    expect(service).toBeTruthy();
  }));
});
