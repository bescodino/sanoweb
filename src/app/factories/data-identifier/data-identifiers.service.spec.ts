import { TestBed, inject } from '@angular/core/testing';

import { DataIdentifiersFactory } from './data-identifiers.service';

describe('DataIdentifiersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataIdentifiersFactory]
    });
  });

  it('should ...', inject([DataIdentifiersFactory], (service: DataIdentifiersFactory) => {
    expect(service).toBeTruthy();
  }));
});
