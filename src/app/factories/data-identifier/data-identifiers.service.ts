import { Injectable } from '@angular/core';

import { ApiSettings } from '../../settings/api.settings';
import { FactoryMapper } from '../../settings/factory.mapper';

import { EntryFactoryService } from '../../factories/entry/entry-factory.service';
import { ContentFactoryService } from '../../factories/content/content-factory.service';

import { IParserDataIdentifiers, IFactoryID } from '../../charts/chart-entities.model';
import {
    DistributorIdentifier, SalesStatusIdentifier,
    AggregateMonthlyIdentifier, DailyIdentifier,
    LeadtimeByMonth, LeadtimeByRegion, LeadtimeByStatus
} from '../../parsers/data-identifiers/data-identifiers.model';

@Injectable()
export class DataIdentifiersFactory {

    private settings = new ApiSettings();
    private factoryMapper = new FactoryMapper();

    constructor() { }

    /**
     * Retorna o parser apropriado para a requisição passada.
     * @param factoryId O mesmo parâmetro usado na requisição feita para adquirir o recurso
     */
    public getDataIdentifier(factoryId: string): IParserDataIdentifiers {

        console.log('DataIdentifiersFactory::getDataIdentifier::starting: ', factoryId);

        switch (factoryId) {

            case this.factoryMapper.distributorsID:
                console.log('Creating \'Distributor\' IParserDataIdentifiers...');
                return new DistributorIdentifier();

            case this.factoryMapper.salesStatusID:
                console.log('Creating \'Sales Status\' IParserDataIdentifiers...');
                return new SalesStatusIdentifier();

            case this.factoryMapper.monthlyID:
                console.log('Creating \'Aggregate Monthly\' IParserDataIdentifiers...');
                return new AggregateMonthlyIdentifier();

            case this.factoryMapper.dailyID:
                console.log('Creating \'Daily\' IParserDataIdentifiers...');
                return new DailyIdentifier();

            case this.factoryMapper.multiDatasetID:
                console.log('Creating \'Daily\' IParserDataIdentifiers...');
                return new LeadtimeByMonth();

            case this.factoryMapper.leadtimeByRegion:
                console.log('Creating \'byRegion\' IParserDataIdentifiers...');
                return new LeadtimeByRegion();

            case this.factoryMapper.leadtimeByStatus:
                console.log('Creating \'byRegion\' IParserDataIdentifiers...');
                return new LeadtimeByStatus();


            default:
                throw new Error('Exception:: no IParserDataIdentifiers could be inferred: bad request ID.');
        }
    }


}
