import { Injectable } from '@angular/core';

import { ApiSettings } from '../../settings/api.settings';
import { FactoryMapper } from '../../settings/factory.mapper';

import { EntryFactoryService } from '../../factories/entry/entry-factory.service';
import { ContentFactoryService } from '../../factories/content/content-factory.service';
import { DataIdentifiersFactory } from '../../factories/data-identifier/data-identifiers.service';

import { IChartParser, IFactoryID } from '../../charts/chart-entities.model';
import {
    ChartCommonParser, ChartTranslateKeyParser,
    ChartAggregateMonthsParser, ChartDailyParser,
    ChartLeadtimeByMonthParser, ChartLeadtimeByRegionParser
} from '../../parsers/chart-parser-impl/chart-parser-impl.component';

import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class ChartsParserFactory {

    private settings = new ApiSettings();
    private factoryMapper = new FactoryMapper();

    constructor(
        private entryFactoryService: EntryFactoryService,
        private contentFactoryService: ContentFactoryService,
        private dataIdentifiersFactory: DataIdentifiersFactory,
        private translate: TranslateService
    ) { }


    /**
     * Retorna o parser apropriado para a requisição passada.
     * @param factoryId O mesmo parâmetro usado na requisição feita para adquirir o recurso
     */
    public getParser(factoryId: string): IChartParser {

        console.log('ChartsParserFactory::getParser::starting: ', factoryId);

        switch (factoryId) {


            case this.factoryMapper.leadtimeByStatus:
            case this.factoryMapper.distributorsID:
                console.log('Creating \'Distributor\' Parser...');
                return new ChartCommonParser(
                    factoryId, this.entryFactoryService, this.contentFactoryService, this.dataIdentifiersFactory);

            case this.factoryMapper.salesStatusID:
                console.log('Creating \'Translate Key\' Parser...');
                return new ChartTranslateKeyParser(
                    factoryId, this.entryFactoryService, this.contentFactoryService,
                    this.dataIdentifiersFactory, this.translate);

            case this.factoryMapper.monthlyID:
                console.log('Creating \'Aggregate Months\' Parser...');
                return new ChartAggregateMonthsParser(
                    factoryId, this.entryFactoryService, this.contentFactoryService, this.dataIdentifiersFactory);

            case this.factoryMapper.dailyID:
                console.log('Creating \'Daily\' Parser...');
                return new ChartDailyParser(
                    factoryId, this.entryFactoryService, this.contentFactoryService, this.dataIdentifiersFactory);

            case this.factoryMapper.multiDatasetID:
                console.log('Creating \'ChartLeadtimeByMonthParser\' Parser...');
                return new ChartLeadtimeByMonthParser(
                    factoryId, this.entryFactoryService, this.contentFactoryService, this.dataIdentifiersFactory);

            case this.factoryMapper.leadtimeByRegion:
                console.log('Creating \'ChartLeadtimeByMonthParser\' Parser...');
                return new ChartLeadtimeByRegionParser(
                    factoryId, this.entryFactoryService, this.contentFactoryService, this.dataIdentifiersFactory);





            default:
                throw new Error('Exception:: no IChartParser could be inferred: bad request ID.');
        }
    }
}
