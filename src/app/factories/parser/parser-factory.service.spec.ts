import { TestBed, inject } from '@angular/core/testing';

import { ChartsParserFactory } from './parser-factory.service';

describe('ChartsParserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChartsParserFactory]
    });
  });

  it('should ...', inject([ChartsParserFactory], (service: ChartsParserFactory) => {
    expect(service).toBeTruthy();
  }));
});
