import { Injectable } from '@angular/core';

// Import Settings
import { ApiSettings } from '../../settings/api.settings';
import { FactoryMapper } from '../../settings/factory.mapper';

// Import interfaces and implementations
import { IChartEntryNEW } from '../../charts/chart-entities.model';
import { ChartCommonEntry, ChartMultiValueEntry, ChartStackedEntry } from '../../charts/charts-implementation';


@Injectable()
export class EntryFactoryService {

    private factoryMapper = new FactoryMapper();

    /**
      * Retorna o IChartEntry apropriado para a requisição passada.
      * @param entryId O mesmo parâmetro usado na requisição feita para adquirir o recurso
      */
    public getEntry(entryId: string): IChartEntryNEW {

        console.log('EntryFactoryService::getEntry::starting: ', entryId);

        switch (entryId) {

            case this.factoryMapper.leadtimeByStatus:
            case this.factoryMapper.dailyID:
            case this.factoryMapper.salesStatusID:
            case this.factoryMapper.distributorsID:
                // console.log('Creating \'Distributor\' ChartEntry...');
                return new ChartCommonEntry();

            case this.factoryMapper.monthlyID:
                // console.log('Creating \'Multi Value\' ChartEntry...');
                return new ChartMultiValueEntry();

            case this.factoryMapper.leadtimeByRegion:
            case this.factoryMapper.multiDatasetID:
                // console.log('Creating \'Multi Value\' ChartEntry...');
                return new ChartStackedEntry();

            default:
                throw new Error('Exception:: no IChartEntry could be inferred: bad request ID.');
        }
    }

}
