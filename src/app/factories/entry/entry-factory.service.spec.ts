import { TestBed, inject } from '@angular/core/testing';

import { EntryFactoryService } from './entry-factory.service';

describe('EntryFactoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EntryFactoryService]
    });
  });

  it('should ...', inject([EntryFactoryService], (service: EntryFactoryService) => {
    expect(service).toBeTruthy();
  }));
});
