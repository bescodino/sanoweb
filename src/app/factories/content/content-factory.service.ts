import { Injectable } from '@angular/core';

// Import Settings
import { ApiSettings } from '../../settings/api.settings';
import { FactoryMapper } from '../../settings/factory.mapper';

// Import interfaces and implementations
import { IChartContentNEW } from '../../charts/chart-entities.model';
import {
    ChartCommonContent, ChartAggregateContent,
    ChartDailyContent, ChartDoubleStackedContent
} from '../../charts/charts-implementation';


@Injectable()
export class ContentFactoryService {

    private factoryMapper = new FactoryMapper();

    /**
      * Retorna o IChartEntry apropriado para a requisição passada.
      * @param contentId O mesmo parâmetro usado na requisição feita para adquirir o recurso
      */
    public getContent(contentId: string): IChartContentNEW {

        console.log('ContentFactoryService::getEntry::starting: ', contentId);

        switch (contentId) {

            case this.factoryMapper.leadtimeByStatus:
            case this.factoryMapper.salesStatusID:
            case this.factoryMapper.distributorsID:
                console.log('Creating \'Distributor\' ChartContent...');
                return new ChartCommonContent();

            case this.factoryMapper.monthlyID:
                console.log('Creating \'Aggregate\' ChartContent...');
                return new ChartAggregateContent();

            case this.factoryMapper.dailyID:
                console.log('Creating \'Daily\' ChartContent...');
                return new ChartDailyContent();

            case this.factoryMapper.leadtimeByRegion:
            case this.factoryMapper.multiDatasetID:
                console.log('Creating \'DoubleStacked\' ChartContent...');
                return new ChartDoubleStackedContent();


            default:
                throw new Error('Exception:: no IChartContent could be inferred: bad request ID.');
        }
    }


}
