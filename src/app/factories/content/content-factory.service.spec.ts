import { TestBed, inject } from '@angular/core/testing';

import { ContentFactoryService } from './content-factory.service';

describe('ContentFactoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContentFactoryService]
    });
  });

  it('should ...', inject([ContentFactoryService], (service: ContentFactoryService) => {
    expect(service).toBeTruthy();
  }));
});
